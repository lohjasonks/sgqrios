//
//  SgQrObject.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation

public class SgQrObject{
    var id      : String;
    var length  : String;
    var data    : String;
    
    init(id: String, length: String, data:String) {
        self.id = id;
        self.length = length;
        self.data = data;
    }
    init(id: Int, length: Int, data:String){
        self.id = String(format: "%02d", id);
        self.length = String(format: "%02d", length);
        self.data = data;
    }
    init(id: Int, length: String, data:String){
        self.id = String(format: "%02d", id);
        self.length = String(format: "%02d", length);
        self.data = data;
    }
    init(id: String, length: Int, data:String){
        self.id = String(format: "%02d", id);
        self.length = String(format: "%02d", length);
        self.data = data;
    }
    
    func lengthMatchesData() -> Bool{
        return Int(length) == data.count;
    }
    
    public func toString() -> String {
        return id + length + data;
    }
}
