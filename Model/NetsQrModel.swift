//
// NetsQrModel.swift
// QrUtility
//
// Created by Jason Loh on 17/11/17.
//

import Foundation

public class NetsQrModel {
    
    static let HEADER = "NETSqpay";
    static let ID_REC_MERCHANT_ID = 0;
    static let ID_REC_MERCHANT_NAME = 1;
    static let ID_REC_TERMINAL_ID = 2;
    static let ID_REC_TXN_AMT = 3;
    static let ID_REC_TIMESTAMP = 4;
    static let ID_REC_BLE_METADATA = 5;
    static let ID_REC_MINMAX_VALUE = 6;
    static let ID_REC_RETRIEVAL_REF = 7;
    static let ID_REC_QR_FORM = 8;
    static let ID_REC_TXN_AMT_QUALIFIER = 9;
    static let ID_REC_WEBQR = 10;
    
    static let LEN_HEADER      = 8;
    static let LEN_VERSION     = 1;
    static let LEN_NR          = 2;
    static let LEN_QR_ISSUER   = 10;
    static let LEN_QR_EXP_DATE = 8;
    static let LEN_SIG_V1      = 8;
    static let LEN_SIG_V0      = 64;
    
    static let LEN_REC_MERCHANT_ID = 15;
    static let LEN_REC_MERCHANT_NAME = 80;
    static let LEN_REC_TERMINAL_ID = 8;
    static let LEN_REC_TXN_AMT = 8;
    static let LEN_REC_TIMESTAMP = 14;
    static let LEN_REC_BLE_METADATA = 40;
    static let LEN_REC_MINMAX_VALUE = 16;
    static let LEN_REC_RETRIEVAL_REF = 16;
    static let LEN_REC_QR_FORM           = 1;
    static let LEN_REC_TXN_AMT_QUALIFIER = 1;
    static let LEN_REC_WEBQR = 1;
    static let LEN_REC_RFU = 99;
    
    
    var header: String?;
    var version: String?;
    var nr: String?;
    var qrIssuer: String?;
    var qrExpDate: String?;
    var records: [NetsQrModel.Record];
    var signature: String?;
    
    init(){
        self.records = [Record]();
    }
    
    public func toString() -> String{
        var outputString = String();
        if let header = header {
            outputString += header;
        }
        if let version = version{
            outputString += version;
        }
        if let nr = nr {
            outputString += nr;
        }
        if let qrIssuer = qrIssuer{
            outputString += qrIssuer;
        }
        if let qrExpDate = qrExpDate {
            outputString += qrExpDate;
        }
        for record in records {
            outputString += record.toString();
        }
        
        if let signature = signature, !signature.isEmpty {
            outputString += signature;
        }
        return outputString;
    }

    public func getHeader() -> String? {
        return header;
    }
    
    public func getVersion() -> String? {
        return version;
    }
    
    public func getNr()  -> String?{
        return nr;
    }
    
    public func getQrIssuer() -> String? {
        return qrIssuer;
    }
    
    public func getQrExpDate() -> String? {
        return qrExpDate;
    }
    
    public func getSignature() -> String? {
        return signature;
    }
    
    public func getMid() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_MERCHANT_ID)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getTid() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_TERMINAL_ID)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getMerchantName() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_MERCHANT_NAME)) {
                print("MerchantName Record: " + record.toString())
                return record.data;
            }
        }
        return nil;
    }
    public func getTxnAmount() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_TXN_AMT)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getTimestamp() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_TIMESTAMP)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getMinMax() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_MINMAX_VALUE)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getTxnRefNumber() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_RETRIEVAL_REF)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getQrForm() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_QR_FORM)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getTxnAmtQualifier() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_TXN_AMT_QUALIFIER)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getWebQr() -> String? {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_WEBQR)) {
                return record.data;
            }
        }
        return nil;
    }
    public func getRecordData(id: Int) -> String? {
        for record in records {
            if record.type == (String(format: "%02d", id)) {
                return record.data;
            }
        }
        return nil;
    }
    public func isTxnReferenceProvidedByUser() -> Bool {
        for record in records {
            if record.type == (String(format: "%02d", NetsQrModel.ID_REC_RETRIEVAL_REF)) {
                return record.data.isEmpty;
            }
        }
        return false;
    }
    public func isWebQr() -> Bool {
        if let webQr = getWebQr() {
            if(webQr == "1"){
                return true
            }
        }
        return false
    }
    
    
    public class Builder {
        private var header: String?;
        private var version: String?;
        private var qrIssuer: String?;
        private var qrExpDate: String?;
        private var mid: String?;
        private var merchantName: String?;
        private var tid: String?;
        private var txnAmout: String?;
        private var txnTimestamp: String?;
        private var minMaxValue: String?;
        private var txnRefNumber: String?;
        private var qrForm: String?;
        private var txnAmountQualifier: String?;
        private var webQr: String?;
        private var secretString: String?;
        private var signature: String?;
        private var unknownRecords: [NetsQrModel.Record];
        
        init(){
            unknownRecords = [Record]();
        }
        
        public func header(input: String) -> Builder {
            self.header = input;
            return self;
        }
        public func version(input: String) -> Builder {
            self.version = input;
            return self;
        }
        public func qrIssuer(input: String) -> Builder {
            self.qrIssuer = input;
            return self;
        }
        public func qrExpDate(input: String) -> Builder {
            self.qrExpDate = input;
            return self;
        }
        public func mid(input: String) -> Builder {
            self.mid = input;
            return self;
        }
        public func merchantName(input: String) -> Builder {
            self.merchantName = input;
            return self;
        }
        public func tid(input: String) -> Builder {
            self.tid = input;
            return self;
        }
        public func txnAmout(input: String) -> Builder {
            self.txnAmout = input;
            return self;
        }
        public func txnTimestamp(input: String) -> Builder {
            self.txnTimestamp = input;
            return self;
        }
        public func minMaxValue(input: String) -> Builder {
            self.minMaxValue = input;
            return self;
        }
        public func txnRefNumber(input: String) -> Builder {
            self.txnRefNumber = input;
            return self;
        }
        public func qrForm(input: String) -> Builder {
            self.qrForm = input;
            return self;
        }
        public func txnAmountQualifier(input: String) -> Builder {
            self.txnAmountQualifier = input;
            return self;
        }
        public func webQr(input: String) -> Builder {
            self.webQr = input
            return self
        }
        public func secretString(input: String) -> Builder {
            self.secretString = input;
            return self;
        }
        public func signature(input: String) -> Builder {
            self.signature = input;
            return self;
        }
        public func addRecord(id: Int, data: String) -> Builder {
            switch id {
            case NetsQrModel.ID_REC_MERCHANT_ID:
                self.mid = data;
            case NetsQrModel.ID_REC_MERCHANT_NAME:
                self.merchantName = data;
            case NetsQrModel.ID_REC_TERMINAL_ID:
                self.tid = data;
            case NetsQrModel.ID_REC_TXN_AMT:
                self.txnAmout = data;
            case NetsQrModel.ID_REC_TIMESTAMP:
                self.txnTimestamp = data;
            case NetsQrModel.ID_REC_MINMAX_VALUE:
                self.minMaxValue = data;
            case NetsQrModel.ID_REC_RETRIEVAL_REF:
                self.txnRefNumber = data;
            case NetsQrModel.ID_REC_QR_FORM:
                self.qrForm = data;
            case NetsQrModel.ID_REC_TXN_AMT_QUALIFIER:
                self.txnAmountQualifier = data;
            case NetsQrModel.ID_REC_WEBQR:
                self.webQr = data;
            default:
                unknownRecords.append(Record(type: id, length: data.count, data: data));
                break;
            }
            return self;
        }
        
        public func build() -> NetsQrModel {
            let netsQrModel = NetsQrModel();
            var numFieldsExcluded = 0;
            netsQrModel.header = self.header;
            netsQrModel.version = self.version;
            netsQrModel.qrIssuer = self.qrIssuer;
            netsQrModel.qrExpDate = self.qrExpDate;
            
            if let data = self.mid {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_MERCHANT_ID, length: data.count, data: data));
            }
            if let data = self.merchantName {
                if version == "0" {
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_MERCHANT_NAME, length: data.count, data: data));
                } else { numFieldsExcluded += 1; }
            }
            if let data = self.tid {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_TERMINAL_ID, length: data.count, data: data));
            }
            if let data = self.txnAmout {
                if version == "0"{
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_TXN_AMT, length: data.count, data: data));
                } else { numFieldsExcluded += 1; }
            }
            if let data = self.txnTimestamp {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_TIMESTAMP, length: data.count, data: data));
            }
            if let data = self.minMaxValue {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_MINMAX_VALUE, length: data.count, data: data));
            }
            if let data = self.txnRefNumber {
                if version == "0" {
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_RETRIEVAL_REF, length: data.count, data: data));
                } else { numFieldsExcluded += 1; }
            }
            if let data = self.qrForm {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_QR_FORM, length: data.count, data: data));
            }
            if let data = self.txnAmountQualifier {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_TXN_AMT_QUALIFIER, length: data.count, data: data));
            }
            if let data = self.webQr {
                netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_WEBQR, length: data.count, data: data));
            }
            
            unknownRecords.forEach { (record) in
                netsQrModel.records.append(record);
            }
            
            var numRecords = netsQrModel.records.count;
            if version == "1" {
                numRecords += numFieldsExcluded;
            }
            netsQrModel.nr = String.init(format: "%02d",numRecords);
            
            if let data = signature {
                let fullPadding = "0000000000000000000000000000000000000000000000000000000000000000";
                var padLen = 0;
                if data.count < NetsQrModel.LEN_SIG_V0 {
                    padLen = NetsQrModel.LEN_SIG_V0 - data.count;
                }
                netsQrModel.signature = data + QrUtils.getSubstring(input: fullPadding, start: 0, end: padLen);
                
            } else {
                if let _secretString = self.secretString, let _version = self.version {
                    netsQrModel.signature = "";
                    let preSignatureOutput = netsQrModel.toString();
                    netsQrModel.signature = generateSignature(secretString: _secretString, version: _version, data: preSignatureOutput);
                } else {
                    netsQrModel.signature = "0000000000000000000000000000000000000000000000000000000000000000";
                }
            }
            
            if version == "1"{
                if let data = self.merchantName {
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_MERCHANT_NAME, length: data.count, data: data));
                }
                if let data = self.txnAmout {
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_TXN_AMT, length: data.count, data: data));
                }
                if let data = self.txnRefNumber {
                    netsQrModel.records.append(Record(type: NetsQrModel.ID_REC_RETRIEVAL_REF, length: data.count, data: data));
                }
                netsQrModel.records.sort(by: {$0.type < $1.type});
            }
            
            
            return netsQrModel;
        }
        
        private func generateSignature(secretString: String, version: String, data: String) -> String {
            let dataWithSecret = data + secretString;
            
            let getDigest = {(input: NSData) -> NSData in
                let digestLength = Int(CC_SHA256_DIGEST_LENGTH);
                var hashValue = [UInt8](repeating: 0, count: digestLength);
                CC_SHA256(input.bytes, UInt32(input.length), &hashValue);
                return NSData(bytes: hashValue, length: digestLength);
            }
            
            let getHexString = {(input: NSData) -> String in
                var bytes = [UInt8](repeating: 0, count: input.length);
                input.getBytes(&bytes, length: input.length);
                
                var hexString = "";
                for byte in bytes {
                    hexString += String(format:"%02x", UInt8(byte));
                }
                return hexString;
            }
            
            let getSha256HexString = {(input: String) -> String in
                guard let data = input.data(using: .utf8) else {
                    return "";
                }
                return getHexString(getDigest(data as NSData));
            }
            
            var generatedSignature = getSha256HexString(dataWithSecret);
            if version == "1" {
                generatedSignature = QrUtils.getSubstring(input: generatedSignature, start: 0, end: 8) + "00000000000000000000000000000000000000000000000000000000";
            }
            return generatedSignature;
        }
    }
    
    
    class Record {
        var type: String;
        var length: String;
        var data: String;
        
        init(type: String, length: String, data: String) {
            self.type = type;
            self.length = length;
            self.data = data;
        }
        init(type: Int, length: Int, data: String){
            self.type = String(format: "%02d", type);
            self.length = String(format: "%02d", length);
            self.data = data;
        }
        init(type: String, length: Int, data: String){
            self.type = type;
            self.length = String(format: "%02d", length);
            self.data = data;
        }
        init(type:Int, length: String, data: String){
            self.type = String(format: "%02d", type);
            self.length = length;
            self.data = data;
        }
        
        public func toString() -> String {
            return type + length + data;
        }
        
        
        
        
        
        
    }
    
}
