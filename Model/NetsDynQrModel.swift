//
//  NetsDynQrModel.swift
//  QrUtility
//
//  Created by Jason Loh on 22/2/18.
//  Copyright © 2018 BixelTest. All rights reserved.
//

import Foundation

public class NetsDynQrModel {
    
    // MARK: - Length Constants
    private static let LEN_HEADER         = 8;
    private static let LEN_VERSION        = 1;
    private static let LEN_TID            = 8;
    private static let LEN_MID            = 15;
    private static let LEN_STAN           = 6;
    private static let LEN_TIME_STAMP     = 8;
    private static let LEN_OP             = 1;
    private static let LEN_CHANNEL        = 1;
    private static let LEN_AMT            = 8;
    private static let LEN_PAYMENT_ACCEPT = 4;
    private static let LEN_CURRENCY       = 1;
    private static let LEN_OTRS           = 8;
    private static let LEN_MERCHANT_NAME  = 17;
    private static let LEN_CHECKSUM       = 4;
    
    public static let HEADER = "NETSQPAY";
    public static let MIN_LENGTH = 86;
    public static let LENGTHS = [LEN_HEADER,
                                 LEN_VERSION,
                                 LEN_TID,
                                 LEN_MID,
                                 LEN_STAN,
                                 LEN_TIME_STAMP,
                                 LEN_OP,
                                 LEN_CHANNEL,
                                 LEN_AMT,
                                 LEN_PAYMENT_ACCEPT,
                                 LEN_CURRENCY,
                                 LEN_OTRS,
                                 LEN_MERCHANT_NAME,
                                 LEN_CHECKSUM];
    
    //MARK: - Member Variables
    var header : String?;
    var version : String?;
    var tid : String?;
    var mid : String?;
    var stan : String?;
    var timeStamp : String?;
    var op : String?;
    var channel : String?;
    var amt : String?;
    var paymentAccept : String?;
    var currency : String?;
    var otrs : String?;
    var merchantName : String?;
    var checksum : String?;
    
    init() {
    }
    
    //MARK: - Getters
    func getHeader() -> String? {
        return header;
    }
    func getVersion()  -> String? {
        return version;
    }
    func getTid()  -> String? {
        return tid;
    }
    func getMid()  -> String? {
        return mid;
    }
    func getStan()  -> String? {
        return stan;
    }
    func getTimeStamp()  -> String? {
        return timeStamp;
    }
    func getOp() -> String?  {
        return op;
    }
    func getChannel() -> String?  {
        return channel;
    }
    func getAmt() -> String?  {
        return amt;
    }
    func getPaymentAccept()  -> String? {
        return paymentAccept;
    }
    func getCurrency()  -> String? {
        return currency;
    }
    func getOtrs()  -> String? {
        return otrs;
    }
    func getMerchantName()  -> String? {
        return merchantName;
    }
    func getChecksum()  -> String? {
        return checksum;
    }
    
    
    
    func toString() -> String {
        let FILLER_HEX  = "################";
        let FILLER_ZERO = "000000000";
        
        var sb = String();
        if let header = header {
            sb.append(header);
        }
        if let version = version {
            sb.append(version);
        }
        if let tid = tid {
            if tid.count < NetsDynQrModel.LEN_TID {
                let padding = QrUtils.getSubstring(input: FILLER_HEX,
                                                   start: 0,
                                                   end: NetsDynQrModel.LEN_TID - tid.count);
                sb.append(padding);
            }
            sb.append(tid);
        }
        if let mid = mid {
            if  mid.count < NetsDynQrModel.LEN_MID {
                let padding = QrUtils.getSubstring(input: FILLER_HEX,
                                                   start: 0,
                                                   end: NetsDynQrModel.LEN_MID - mid.count);
                sb.append(padding);
            }
            sb.append(mid);
        }
        if let stan = stan{
            sb.append(stan);
        }
        if let timeStamp = timeStamp {
            sb.append(timeStamp);
        }
        if let op = op {
            sb.append(op);
        }
        if let channel = channel {
            sb.append(channel);
        }
        if let amt = amt {
            if amt.count < NetsDynQrModel.LEN_AMT {
                let padding = QrUtils.getSubstring(input: FILLER_ZERO,
                                                   start: 0,
                                                   end: NetsDynQrModel.LEN_AMT - amt.count);
                sb.append(padding);
            }
            sb.append(amt);
        }
        if let paymentAccept = paymentAccept {
            sb.append(paymentAccept);
        }
        if let currency = currency {
            sb.append(currency);
        }
        if let otrs = otrs {
            sb.append(otrs);
        }
        if let merchantName = merchantName {
            if merchantName.count > 17 {
                sb.append(QrUtils.getSubstring(input: merchantName,
                                               start: 0,
                                               end: 17));
            }
            sb.append(merchantName);
            
        }
        if let checksum = checksum{
            sb.append(checksum);
        }
        return sb;
    }
    //TODO: - Add Builder
    
    public class Builder {
        var header : String? = NetsDynQrModel.HEADER;
        var version : String? = "0";
        var tid : String?;
        var mid : String?;
        var stan : String?;
        var timeStamp : String?;
        var op : String?;
        var channel : String?;
        var amt : String?;
        var paymentAccept : String?;
        var currency : String? = "0";
        var otrs : String?;
        var merchantName : String?;
        var checksum : String? = "";
        
        
        public func header(input: String) -> Builder {
            self.header = input;
            return self;
        }
        public func version(input: String) -> Builder {
            self.version = input;
            return self;
        }
        public func tid(input: String) -> Builder {
            self.tid = input;
            return self;
        }
        public func mid(input: String) -> Builder {
            self.mid = input;
            return self;
        }
        public func stan(input: String) -> Builder {
            self.stan = input;
            return self;
        }
        public func timeStamp(input: String) -> Builder {
            self.timeStamp = input;
            return self;
        }
        public func op(input: String) -> Builder {
            self.op = input;
            return self;
        }
        public func channel(input: String) -> Builder {
            self.channel = input;
            return self;
        }
        public func amt(input: String) -> Builder {
            self.amt = input;
            return self;
        }
        public func paymentAccept(input: String) -> Builder {
            self.paymentAccept = input;
            return self;
        }
        public func currency(input: String) -> Builder {
            self.currency = input;
            return self;
        }
        public func otrs(input: String) -> Builder {
            self.otrs = input;
            return self;
        }
        public func merchantName(input: String) -> Builder {
            self.merchantName = input;
            return self;
        }
        public func checksum(input: String) -> Builder {
            self.checksum = input;
            return self;
        }

        public func build() -> NetsDynQrModel{
            let model = NetsDynQrModel();
            model.header = self.header;
            model.version = self.version;
            model.tid = self.tid;
            model.mid =  self.mid;
            model.stan = self.stan;
            if let temp = timeStamp, !temp.isEmpty {
                model.timeStamp = temp;
            } else {
                model.timeStamp = getCurrentTimeStamp();
            }
            model.op = self.op;
            model.channel = self.channel;
            model.amt = self.amt;
            model.paymentAccept = self.paymentAccept;
            model.currency = self.currency;
            model.otrs = self.otrs;
            model.merchantName = self.merchantName;
            let preCrcString = model.toString();
            if let temp = checksum, !temp.isEmpty {
                model.checksum = temp;
            } else {
                model.checksum = String(format: "%04x", CrcUtils.crc16(data: [UInt8](preCrcString.utf8)));
            }
            return model;
        }
        
        private func getCurrentTimeStamp() -> String {
            let zeros = "00000000";
            let currTime = Date().timeIntervalSince1970;
            let julianDateTime = currTime + 8 * 60 * 60 - 9131 * 86400;
            let timeString = String(format: "%08X", Int(julianDateTime));
            return timeString;
        }
        
    }

    
}
