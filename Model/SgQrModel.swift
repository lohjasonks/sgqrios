//
//  SgQrModel.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//

import Foundation

public class SgQrModel {
    var payloadFormatIndicator              : String?;
    var pointOfInitiationMethod             : String?;
    var merchantAcctInfoEmv                 : [Int: SgQrObject];
    var merchantAcctInfoSg                  : [Int: SgQrObject];
    var sgQrIdentityInformation             : String?;
    var merchantCategoryCode                : String?;
    var transactionCurrency                 : String?;
    var transactionAmount                   : String?;
    var tipOrConvenienceIndicator           : String?;
    var valueOfConvenienceFeeFixed          : String?;
    var valueOfConvenienceFeePercentage     : String?;
    var merchantCountryCode                 : String?;
    var merchantName                        : String?;
    var merchantCity                        : String?;
    var postalCode                          : String?;
    var additionalData                      : [Int: SgQrObject];
    var merchantInformationLanguageTemplate : [Int: SgQrObject];
    var rfuForEmv                           : [Int: SgQrObject];
    var unreservedTemplates                 : [Int: SgQrObject];
    var crc                                 : String?;
    
    init(){
        self.merchantAcctInfoEmv                    = [Int: SgQrObject]();
        self.merchantAcctInfoSg                     = [Int: SgQrObject]();
        self.additionalData                         = [Int: SgQrObject]();
        self.merchantInformationLanguageTemplate    = [Int: SgQrObject]();
        self.rfuForEmv                              = [Int: SgQrObject]();
        self.unreservedTemplates                    = [Int: SgQrObject]();
    }
    
    public func toString() -> String {
        var output = String();
        if let data = payloadFormatIndicator, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = pointOfInitiationMethod, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_POINT_OF_INITIATION_METHOD));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        let emvMerchantsInfo = merchantAcctInfoEmv.sorted{$0.value.id < $1.value.id}
        for item in emvMerchantsInfo{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        let sgMerchantsInfo = merchantAcctInfoSg.sorted{$0.value.id < $1.value.id}
        for item in sgMerchantsInfo{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        if let data = sgQrIdentityInformation, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_SGQR_ID_INFORMATION));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = merchantCategoryCode, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_MERCHANT_CATEGORY_CODE));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = transactionCurrency, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_TRANSACTION_CURRENCY));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = transactionAmount, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_TRANSACTION_AMOUNT));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = tipOrConvenienceIndicator, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = valueOfConvenienceFeeFixed, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = valueOfConvenienceFeePercentage, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = merchantCountryCode, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_COUNTRY_CODE));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = merchantName, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_MERCHANT_NAME));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        
        if let data = merchantCity, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_MERCHANT_CITY));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        if let data = postalCode, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_POSTAL_CODE));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        let addnData = additionalData.sorted{$0.value.id < $1.value.id}
        var lenAddnData = 0;
        for item in addnData{
            lenAddnData += item.value.data.count;
        }
        if lenAddnData > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA));
            output.append(String(format: "%02d", lenAddnData));
        }
        for item in addnData{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        
        let merLang = merchantInformationLanguageTemplate.sorted{$0.value.id < $1.value.id}
        var lenMerLangData = 0;
        for item in merLang{
            lenMerLangData += item.value.data.count;
        }
        if lenMerLangData > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE));
            output.append(String(format: "%02d", lenMerLangData));
        }
        for item in merLang{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        let rfu = rfuForEmv.sorted{$0.value.id < $1.value.id}
        for item in rfu{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        let unreserved = unreservedTemplates.sorted{$0.value.id < $1.value.id}
        for item in unreserved{
            output.append(item.value.id);
            output.append(item.value.length);
            output.append(item.value.data);
        }
        if let data = crc, data.count > 0 {
            output.append(String(format: "%02d", SgQrConsts.ID_CRC));
            output.append(String(format: "%02d", data.count));
            output.append(data);
        }
        
        return output;
    }
    
    public func getPayloadFormatIndicator() -> String? {
        return self.payloadFormatIndicator;
    }
    public func getPointOfInitiationMethod() -> String? {
        return self.pointOfInitiationMethod;
    }
    public func getMerchantAcctInfoEmv() -> [Int: SgQrObject]{
        return self.merchantAcctInfoEmv;
    }
    public func getMerchantAcctInfoSg() -> [Int: SgQrObject]{
        return self.merchantAcctInfoSg;
    }
    public func getSgQrIdentityInformation() -> String? {
        return self.sgQrIdentityInformation;
    }
    public func getMerchantCategoryCode() -> String? {
        return self.merchantCategoryCode;
    }
    public func getTransactionCurrency() -> String? {
        return self.transactionCurrency;
    }
    public func getTransactionAmount() -> String? {
        return self.transactionAmount;
    }
    public func getTipOrConvenienceIndicator() -> String? {
        return self.tipOrConvenienceIndicator;
    }
    public func getValueOfConvenienceFeeFixed() -> String? {
        return self.valueOfConvenienceFeeFixed;
    }
    public func getValueOfConvenienceFeePercentage() -> String? {
        return self.valueOfConvenienceFeePercentage;
    }
    public func getMerchantCountryCode() -> String? {
        return self.merchantCountryCode;
    }
    public func getMerchantName() -> String? {
        return self.merchantName;
    }
    public func getMerchantCity() -> String? {
        return self.merchantCity;
    }
    public func getPostalCode() -> String? {
        return self.postalCode;
    }
    public func getAdditionalData() -> [Int: SgQrObject]{
        return self.additionalData;
    }
    public func getMerchantInformationLanguageTemplate() -> [Int: SgQrObject]{
        return self.merchantInformationLanguageTemplate;
    }
    public func getRfuForEmv() -> [Int: SgQrObject]{
        return self.rfuForEmv;
    }
    public func getUnreservedTemplates() -> [Int: SgQrObject]{
        return self.unreservedTemplates;
    }
    public func getCrc() -> String? {
        return self.crc;
    }
    public func getAdtBillNumber() -> String? {
        if let temp = additionalData[SgQrConsts.AddnData.ID_BILL_NUMBER] {
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtMobileNumber() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_MOBILE_NUMBER]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtLoyaltyNumber() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_LOYALTY_NUMBER]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getrAdtStoreLabel() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_STORE_LABEL]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtReferenceLabel() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_REFERENCE_LABEL]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtCustomerLabel() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_CUSTOMER_LABEL]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtTerminalLabel() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_TERMINAL_LABEL]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtPurposeOfTxn() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_PURPOSE_OF_TXN]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtAddnCustDataReq() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_ADDN_CUST_DATA_REQ]{
            return temp.data;
        } else {
            return nil;
        }
    }
    public func getAdtPaymentSpecificTemplates() -> String?{
        if let temp = additionalData[SgQrConsts.AddnData.ID_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES]{
            return temp.data;
        } else {
            return nil;
        }
    }
    
    public class Builder {
        
        public static let PAYLOAD_FORMAT_INDICATOR_DEFAULT = "01";
        public static let POINT_OF_INITIATION_STATIC       = "11";
        public static let POINT_OF_INITIATION_DYNAMIC      = "12";
        public static let MERCHANT_CAT_CODE_DEFAULT        = "0000";
        public static let TXN_CURRENCY_DEFAULT             = "702";
        public static let MERCHANT_COUNTRY_CODE_DEFAULT    = "SG";
        public static let MERCHANT_CITY_DEFAULT            = "SINGAPORE";
        
        var payloadFormatIndicator              : String?;
        var pointOfInitiationMethod             : String?;
        var merchantAcctInfoEmv                 : [Int: SgQrObject];
        var merchantAcctInfoSg                  : [Int: SgQrObject];
        var sgQrIdentityInformation             : String?;
        var merchantCategoryCode                : String?;
        var transactionCurrency                 : String?;
        var transactionAmount                   : String?;
        var tipOrConvenienceIndicator           : String?;
        var valueOfConvenienceFeeFixed          : String?;
        var valueOfConvenienceFeePercentage     : String?;
        var merchantCountryCode                 : String?;
        var merchantName                        : String?;
        var merchantCity                        : String?;
        var postalCode                          : String?;
        var additionalData                      : [Int: SgQrObject];
        var merchantInformationLanguageTemplate : [Int: SgQrObject];
        var rfuForEmv                           : [Int: SgQrObject];
        var unreservedTemplates                 : [Int: SgQrObject];
        var crc                                 : String?;
        
        init (){
            self.merchantAcctInfoEmv                    = [Int: SgQrObject]();
            self.merchantAcctInfoSg                     = [Int: SgQrObject]();
            self.additionalData                         = [Int: SgQrObject]();
            self.merchantInformationLanguageTemplate    = [Int: SgQrObject]();
            self.rfuForEmv                              = [Int: SgQrObject]();
            self.unreservedTemplates                    = [Int: SgQrObject]();
        }
        
        public func payloadFormatIndicator(input: String?) -> Builder {
            self.payloadFormatIndicator = input;
            return self;
        }
        public func pointOfInitiationMethod(input: String?) -> Builder {
            self.pointOfInitiationMethod = input;
            return self;
        }
        public func sgQrIdentityInformation(input: String?) -> Builder {
            self.sgQrIdentityInformation = input;
            return self;
        }
        public func merchantCategoryCode(input: String?) -> Builder {
            self.merchantCategoryCode = input;
            return self;
        }
        public func transactionCurrency(input: String?) -> Builder {
            self.transactionCurrency = input;
            return self;
        }
        public func transactionAmount(input: String?) -> Builder {
            self.transactionAmount = input;
            return self;
        }
        public func tipOrConvenienceIndicator(input: String?) -> Builder {
            self.tipOrConvenienceIndicator = input;
            return self;
        }
        public func valueOfConvenienceFeePercentage(input: String?) -> Builder {
            self.valueOfConvenienceFeePercentage = input;
            return self;
        }
        public func valueOfConvenienceFeeFixed(input: String?) -> Builder {
            self.valueOfConvenienceFeeFixed = input;
            return self;
        }
        public func merchantCountryCode(input: String?) -> Builder {
            self.merchantCountryCode = input;
            return self;
        }
        public func merchantName(input: String?) -> Builder {
            self.merchantName = input;
            return self;
        }
        public func merchantCity(input: String?) -> Builder {
            self.merchantCity = input;
            return self;
        }
        public func postalCode(input: String?) -> Builder {
            self.postalCode = input;
            return self;
        }
        public func crc(input: String?) -> Builder {
            self.crc = input;
            return self;
        }
        public func addAcctInfoEmv(id: Int, data: SgQrObject) -> Builder {
            self.merchantAcctInfoEmv[id] = data;
            return self;
        }
        public func addAcctInfoSg(id: Int, data: SgQrObject) -> Builder {
            self.merchantAcctInfoSg[id] = data;
            return self;
        }
        public func addAdditionalData(id: Int, data: SgQrObject) -> Builder {
            self.additionalData[id] = data;
            return self;
        }
        public func addMerchantInfoLanguageTemplate(id: Int, data: SgQrObject) -> Builder {
            self.merchantInformationLanguageTemplate[id] = data;
            return self;
        }
        public func setAdditionalData(input: [Int: SgQrObject]) -> Builder {
            self.additionalData = input;
            return self;
        }
        public func setMerchantInfoLanguageTemplate(input: [Int: SgQrObject]) -> Builder {
            self.merchantInformationLanguageTemplate = input;
            return self;
        }
        public func addRfuForEmv(id: Int, data: SgQrObject) -> Builder {
            self.rfuForEmv[id] = data;
            return self;
        }
        public func addUnreservedTemplates(id: Int, data: SgQrObject) -> Builder {
            self.unreservedTemplates[id] = data;
            return self;
        }
        
        public func build() -> SgQrModel {
            let sgQr = SgQrModel();
            
            if let data = self.payloadFormatIndicator {
                sgQr.payloadFormatIndicator = data;
            } else {
                sgQr.payloadFormatIndicator = Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT;
            }
            
            
            if let data = self.pointOfInitiationMethod {
                sgQr.pointOfInitiationMethod = data;
            }
            
            if let data = self.sgQrIdentityInformation {
                sgQr.sgQrIdentityInformation = data;
            } else {
                sgQrIdentityInformation = "";
            }
            
            if let data = self.merchantCategoryCode {
                sgQr.merchantCategoryCode = data;
            } else {
                sgQr.merchantCategoryCode = Builder.MERCHANT_CAT_CODE_DEFAULT;
            }
            
            if let data = transactionCurrency {
                sgQr.transactionCurrency = data;
            } else {
                sgQr.transactionCurrency = Builder.TXN_CURRENCY_DEFAULT;
            }
            
            sgQr.merchantAcctInfoEmv = self.merchantAcctInfoEmv;
            sgQr.merchantAcctInfoSg = self.merchantAcctInfoSg;
            
            if let data = self.transactionAmount {
                sgQr.transactionAmount = data;
            }
            
            if let data = self.tipOrConvenienceIndicator {
                sgQr.tipOrConvenienceIndicator = data;
                if let fixedFee = self.valueOfConvenienceFeeFixed {
                    sgQr.valueOfConvenienceFeeFixed = fixedFee;
                } else {
                    sgQr.valueOfConvenienceFeeFixed = "";
                }
                if let percentFee = self.valueOfConvenienceFeePercentage {
                    sgQr.valueOfConvenienceFeePercentage = percentFee;
                } else {
                    sgQr.valueOfConvenienceFeePercentage = "";
                }
            }
            
            if let data = self.merchantCountryCode {
                sgQr.merchantCountryCode = data;
            } else {
                sgQr.merchantCountryCode = Builder.MERCHANT_COUNTRY_CODE_DEFAULT;
            }
            
            if let data = self.merchantCity {
                sgQr.merchantCity = data;
            } else {
                sgQr.merchantCity = Builder.MERCHANT_CITY_DEFAULT;
            }
            
            if let data = self.merchantName {
                sgQr.merchantName = data;
            } else {
                sgQr.merchantName = "";
            }
            
            if let data = self.postalCode {
                sgQr.postalCode = data;
            }
            
            sgQr.additionalData = self.additionalData;
            sgQr.merchantInformationLanguageTemplate = self.merchantInformationLanguageTemplate;
            
            var data = sgQr.toString() + "6304";
            if(crc == nil || crc!.isEmpty){
                sgQr.crc = String(format: "%04x", CrcUtils.crc16(data: [UInt8](data.utf8)));
            } else {
                sgQr.crc = crc;
            }
            
            return sgQr;
        }
        
    }
}
