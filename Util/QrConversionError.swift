//
//  QRConversionError.swift
//  QrUtility
//
//  Created by Jason Loh on 1/12/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation
class QrConversionError: Error {
    let errorCode: String;
    let errorMessage: String;
    
    init(code: String, message: String){
        errorCode = code;
        errorMessage = message;
    }
    
    public func getInfo() -> String {
        return "[" + errorCode + "]: " + errorMessage;
    }
}

