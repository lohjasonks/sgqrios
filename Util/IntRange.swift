//
//  IntRange.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation

public class IntRange{
    var min: Int;
    var max: Int;
    
    init(_ min: Int, _ max: Int) {
        self.min = min;
        self.max = max;
    }
    
    func isInRange(_ valueToCheck: Int) -> Bool {
        return min <= valueToCheck && valueToCheck <= max;
    }
}
