//
//  QrUtils.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation


public class QrUtils {
    
    public static func crcIsValid(qrData: String) -> Bool{
        let qrLength        = qrData.count;
        let dataToCrc       = getSubstring(input: qrData,
                                           start: 0,
                                           end: qrLength - 4);
        
        let declaredCrc     = getSubstring(input: qrData,
                                           start: qrLength - 4,
                                           end: qrLength);
        
        let calculatedCrc   = String(format: "%04x",
                                     CrcUtils.crc16(data: [UInt8](dataToCrc.utf8)));
        
        //print("Orig CRC: \(declaredCrc) , Output crc: \(calculatedCrc)");
        
        return declaredCrc.caseInsensitiveCompare(calculatedCrc) == .orderedSame;
    }
    
    //Check if Provided string is valid SGQR Data
    public static func isValidSgQrString(qrData: String) throws -> Bool {
        let qrLength        = qrData.count;
        let idFieldLen      = 2;
        let lenFieldLen     = 2;
        var mandatoryFields = [Int]();
        var entryExists     = [Int: Bool]();
        
        initMandatoryFields(list: &mandatoryFields);
        let idMap = getIdToLengthMap();
        
        //Check does CRC ID and length field match
        let crcIdAndLen = getSubstring(input: qrData,
                                       start: qrLength - 8,
                                       end: qrLength - 4);
        
        let declaredIdAndLen = String(format: "%02d", SgQrConsts.ID_CRC) + "04";
        let crcIdLenValid = crcIdAndLen.caseInsensitiveCompare(declaredIdAndLen) != .orderedSame;
        
        if  crcIdLenValid {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                    detail: ErrorCodes.DET_CRC_IDLEN_INVALID),
                                    message: "Check QR: Invalid CRC ID/Len");
        }
        
        //Check crc validity
        let isCrcValid = crcIsValid(qrData: qrData);
        
        if !isCrcValid{
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                          detail: ErrorCodes.DET_CRC_VALUE_INVALID),
                                    message: "Check QR: Invalid CRC Value");
        }
        
        
        
        var index: Int = 0;
        while index < qrLength {
            let id = Int(getSubstring(input: qrData,
                                      start: index,
                                      end: index + idFieldLen));
            index += idFieldLen;
            
            let len = Int(getSubstring(input: qrData,
                                       start: index,
                                       end: index + lenFieldLen));
            index += lenFieldLen;
            
            //Check could parse id and length
            if id == nil || len == nil {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                              detail: ErrorCodes.DET_CANT_PARSE_IDLEN),
                                        message: "Check QR: Could not parse ID/Len to Int");
            }
            
            //Check no duplicate entries
            if entryExists.keys.contains(id!){
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                              detail: ErrorCodes.DET_DUPLICATE_ENTRIES),
                                        message: "Check QR: Duplicate Entry found");
            } else {
                entryExists.updateValue(true, forKey: id!);
            }
            
            //Check Length is valid for given ID
            let isValidLength = isLengthValid(key: id!,
                                              length: len!,
                                              idMap: idMap);
            
            if(!isValidLength){
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                              detail: ErrorCodes.DET_ID_LEN_MISMATCH),
                                        message: "Check QR: Length Does not match ID");
            }
            
            //Check declared length does not exceed total length
            if len! > qrLength - index{
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                              detail: ErrorCodes.DET_LEN_TOO_LONG),
                                        message: "Check QR: Declared length Exceeds total length");
            }
            
            index += len!;
            
            //Check remaining data length is at least 5 if it has not reached end
            if qrLength != index && qrLength - index < 5 {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                              detail: ErrorCodes.DET_REMAINING_LEN_ERR),
                                        message: "Check QR: Remaining data length < 5");
            }
        }
        
        //Check all the mandatory fields are there
        let mandatoryFieldsExist = checkMandatoryFieldsExist(mandatoryFields: mandatoryFields,
                                                             foundFields: entryExists);
        
        if !mandatoryFieldsExist {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_SGQR,
                                                          detail: ErrorCodes.DET_MANDATORY_FIELD_MISSING),
                                    message: "Check QR: One or more mandatory fields is missing");
        }
        
        return true;
    }
    
    // Should be called after calling is QR string valid function
    // SGQR String -> Map of SGQR Objects
    public static func extractSgQrObjectMap(_ qrData: String) -> [Int: SgQrObject] {
        
        var sgQrObjectMap   = [Int: SgQrObject]();
        var index           = 0;
        let qrLength        = qrData.count;
        let idFieldLen      = 2;
        let lenFieldLen     = 2;
        
        //get an SgQrObject each iteration and add it to the map
        while index < qrLength {
            
            // Parse ID from QRCode String
            let id = Int(getSubstring(input : qrData,
                                      start : index,
                                      end   : index + idFieldLen));
            index += idFieldLen;
            
            // Parse Length from QRCode String
            let len = Int(getSubstring(input: qrData,
                                       start: index,
                                       end: index + lenFieldLen));
            index += lenFieldLen;
            
            //If ID & Len OK, get the Data, construct SGQRObj and add it
            if let id = id, let len = len {
                let data   = getSubstring(input: qrData,
                                          start: index,
                                          end: index + len);
                index += len;
                
                let qrObj   = SgQrObject(id : id,
                                         length : len,
                                         data : data)
                
                sgQrObjectMap.updateValue(qrObj, forKey: id);
            }
        }
        return sgQrObjectMap;
    }
    
    //Convert SGQR String -> Map of SGQR Objects -> SGQR Model
    public static func parseSgQrString(qrData: String) -> SgQrModel {
        //        let sgQr = SgQrModel();
        let sgQrObjectMap: [Int: SgQrObject] = extractSgQrObjectMap(qrData);
        let builder = SgQrModel.Builder()
            .payloadFormatIndicator(input: sgQrObjectMap[SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR]?.data)
            .sgQrIdentityInformation(input: sgQrObjectMap[SgQrConsts.ID_SGQR_ID_INFORMATION]?.data)
            .merchantCategoryCode(input: sgQrObjectMap[SgQrConsts.ID_MERCHANT_CATEGORY_CODE]?.data)
            .transactionCurrency(input: sgQrObjectMap[SgQrConsts.ID_TRANSACTION_CURRENCY]?.data)
            .merchantCountryCode(input: sgQrObjectMap[SgQrConsts.ID_COUNTRY_CODE]?.data)
            .merchantName(input: sgQrObjectMap[SgQrConsts.ID_MERCHANT_NAME]?.data)
            .merchantCity(input: sgQrObjectMap[SgQrConsts.ID_MERCHANT_CITY]?.data)
            .crc(input: sgQrObjectMap[SgQrConsts.ID_CRC]?.data);
        
        _ = builder.pointOfInitiationMethod(input: sgQrObjectMap[SgQrConsts.ID_POINT_OF_INITIATION_METHOD]?.data)
            .transactionAmount(input: sgQrObjectMap[SgQrConsts.ID_TRANSACTION_AMOUNT]?.data)
            .tipOrConvenienceIndicator(input: sgQrObjectMap[SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR]?.data)
            .valueOfConvenienceFeeFixed(input: sgQrObjectMap[SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED]?.data)
            .valueOfConvenienceFeePercentage(input: sgQrObjectMap[SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE]?.data)
            .postalCode(input: sgQrObjectMap[SgQrConsts.ID_POSTAL_CODE]?.data)
        
        
        
        //Assign template fields if they exist
        if let tempObject = sgQrObjectMap[SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA] {
            let addnTemplateObjs = extractSgQrObjectMap(tempObject.data);
            _ = builder.setAdditionalData(input: addnTemplateObjs);
        }
        if let tempObject = sgQrObjectMap[SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE] {
            let merchInfoLangObjs = extractSgQrObjectMap(tempObject.data);
            _ = builder.setMerchantInfoLanguageTemplate(input: merchInfoLangObjs);
        }
        
        //Assign template fields if they exist
        for i in SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV ..< SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV + SgQrConsts.NUM_MERCHANTS_EMV {
            if let tempObject = sgQrObjectMap[i] {
                _ = builder.addAcctInfoEmv(id: i, data: tempObject);
            }
        }
        for i in SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG ..< SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG + SgQrConsts.NUM_MERCHANTS_SG {
            if let tempObject = sgQrObjectMap[i] {
                _ = builder.addAcctInfoSg(id: i, data: tempObject);
            }
        }
        for i in SgQrConsts.ID_RFU_FOR_EMV ..< SgQrConsts.ID_UNRESERVED_TEMPLATES {
            if let tempObject = sgQrObjectMap[i] {
                _ = builder.addRfuForEmv(id: i, data: tempObject);
            }
        }
        for i in SgQrConsts.ID_UNRESERVED_TEMPLATES ..< SgQrConsts.MAX_ID_VAL + 1 {
            if let tempObject = sgQrObjectMap[i] {
                _ = builder.addUnreservedTemplates(id: i, data: tempObject);
            }
        }
        //        for i in SgQrConsts.ID_RFU_FOR_EMV ..< SgQrConsts.ID_RFU_FOR_EMV + SgQrConsts.NUM_RFU_FOR_EMV {
        //            if let tempObject = sgQrObjectMap[i] {
        //                sgQr.rfuForEmv.updateValue(tempObject, forKey: i);
        //            }
        //        }
        //        for i in SgQrConsts.ID_UNRESERVED_TEMPLATES ..< SgQrConsts.ID_UNRESERVED_TEMPLATES + SgQrConsts.NUM_UNRESERVED_TEMPLATES {
        //            if let tempObject = sgQrObjectMap[i] {
        //                sgQr.unreservedTemplates.updateValue(tempObject, forKey: i);
        //            }
        //        }
        return builder.build();
        //        return sgQr;
    }
    
    //Convert SGQR Model -> NETS QR Model
    public static func convertSgQrModelToNetsQrModel(sgQrModel: SgQrModel) throws -> NetsQrModel {
        //        let netsQr = NetsQrModel();
        let builder = NetsQrModel.Builder();
        
        //Get the relevant NETS MerchantInfo from the SGQR item
        if let netsDataObj = findNetsMerchantAcctInfo(sgMerchantInfoMap: sgQrModel.merchantAcctInfoSg) {
//        if let netsDataObj = sgQrModel.merchantAcctInfoSg[SgQrConsts.MerchantInfo.NETS.MERCHANT_ACCOUNT_ID] {
            let netsAcctInfo: [Int: SgQrObject] = extractSgQrObjectMap(netsDataObj.data);
            _ = builder.header(input: NetsQrModel.HEADER)
                .version(input: "1");
            
            //Mandatory fields
            if sgQrModel.getTransactionCurrency() == nil || sgQrModel.getTransactionCurrency() != "702" {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                              detail: ErrorCodes.DET_UNACCEPTED_CURRENCY_CODE),
                                        message: "SGQRModel->NETSQRModel: Invalid currency: \(sgQrModel.getTransactionCurrency)");
            }
            
            
            if let temp = netsAcctInfo[SgQrConsts.MerchantInfo.NETS.ID_QR_METADATA] {
                let qrMetadata  = temp.data;
                _ = builder.qrIssuer(input: getSubstring(input: qrMetadata,
                                                         start: 1,
                                                         end: 11));
                
                let year        = getSubstring(input: qrMetadata,
                                               start: 11,
                                               end: 13);
                
                let month       = getSubstring(input: qrMetadata,
                                               start: 13,
                                               end: 15);
                
                let day         = getSubstring(input: qrMetadata,
                                               start: 15,
                                               end: 17);
                
                //To get format DDMMYYYY
                _ = builder.qrExpDate(input: day + month + "20" + year);
                
            } else {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                              detail: ErrorCodes.DET_QR_METADATA_NULL),
                                        message: "SGQRModel->NETSQRModel: Error in QR Metadata");
            }
            
            //Mandatory records
            if let temp = netsAcctInfo[SgQrConsts.MerchantInfo.NETS.ID_MERCHANT_ID] {
                _ = builder.mid(input: temp.data);
            } else {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                              detail: ErrorCodes.DET_MID_MISSING),
                                        message: "SGQRModel->NETSQRModel: Error in Merchant ID");
            }
            
            if let temp = sgQrModel.merchantName {
                if(temp.count > NetsQrModel.LEN_REC_MERCHANT_NAME){
                    _ = builder.merchantName(input: getSubstring(input: temp,
                                                                 start: 0,
                                                                 end: NetsQrModel.LEN_REC_MERCHANT_NAME))
                } else {
                    _ = builder.merchantName(input: temp);
                }
            } else {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                              detail: ErrorCodes.DET_MERCH_NAME_MISSING),
                                        message: "SGQRModel->NETSQRModel: Error in Merchant Name");
            }
            
            //Optional records
            if let temp = netsAcctInfo[SgQrConsts.MerchantInfo.NETS.ID_TERMINAL_ID] {
                _ = builder.tid(input: temp.data);
            }
            if let temp = sgQrModel.transactionAmount {
                let finalAmtString = try convertSgQrAmtToNetsQrAmt(amount: temp)
                
                _ = builder.txnAmout(input: finalAmtString);
//                _ = builder.txnAmout(input: temp);
            }
            
            if let temp = sgQrModel.getAdtBillNumber() {
                if temp.count > 16 {
                    throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                  detail: ErrorCodes.DET_TXN_REF_INVALID),
                                            message: "Txn Reference Error: Number Too Long - \(temp.count), Max = \(NetsQrModel.LEN_REC_RETRIEVAL_REF)");
                }
                if temp == "***"{
                    _ = builder.txnRefNumber(input: "");
                } else if !stringIsAlphaNum(input: temp) {
                    throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                  detail: ErrorCodes.DET_TXN_REF_INVALID),
                                            message: "Txn Reference Error: Contains Invalid Character(Non a-z,A-Z,0-9");
                } else {
                    _ = builder.txnRefNumber(input: temp);
                }
            }
            
            if let temp = netsAcctInfo[SgQrConsts.MerchantInfo.NETS.ID_TXN_AMT_MODIFIER] {
                _ = builder.txnAmountQualifier(input: temp.data);
            }
            
            //Copy over signature
            if let temp = netsAcctInfo[SgQrConsts.MerchantInfo.NETS.ID_SIGNATURE] {
                _ = builder.signature(input: temp.data);
            } else {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                              detail: ErrorCodes.DET_SIG_MISSING),
                                        message: "SGQRModel->NETSQRModel: Error in Signature");
            }
        } else {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                          detail: ErrorCodes.DET_NETS_MI_MISSING),
                                    message: "convertSgQrModelToNetsQrModel: Could not find NETS Merchant Info");
        }
        return builder.build();
        //        return netsQr;
    }
    
    private static func convertSgQrAmtToNetsQrAmt(amount: String) throws -> String {
        let errorCode = ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                        detail: ErrorCodes.DET_AMT_INVALID_FOR_NETSQRM)
        
        var amountSubStrings = amount.components(separatedBy: ".")
    
        if(amountSubStrings.count > 2){
            throw QrConversionError(code: errorCode,
                                    message: "convertSgQrModelToNetsQrModel : Amount has too many decimal points");
        }
        for substring in amountSubStrings {
            let isNumeric = Int(substring)
            if substring.count != 0 && isNumeric == nil {
                throw QrConversionError(code: errorCode,
                                        message: "Invalid Amount format: Is not fully numeric")
            }
        }
        if amountSubStrings.count == 2 {
            if amountSubStrings[1].count > 2 {
                throw QrConversionError(code: errorCode,
                                        message: "Invalid Amount format: Amount has too many chars after the decimal")
            } else if (amountSubStrings[1].count != 2){
                let missingLength = 2 - amountSubStrings[1].count
                amountSubStrings[1] = amountSubStrings[1] + getSubstring(input: "00",
                                                                         start: 0,
                                                                         end: missingLength)
            }
        }
        
        var convertedAmount = amountSubStrings[0]
        if amountSubStrings.count == 1{
            convertedAmount = convertedAmount + "00"
        } else {
            convertedAmount = convertedAmount + amountSubStrings[1]
        }
        
        let convertedAmountInt = Int(convertedAmount)
        if convertedAmountInt == nil || convertedAmountInt! > 99999999 {
            throw QrConversionError(code: "0002 0006",
                                    message: "Invalid Amount format: Amount \(convertedAmountInt!) is too large")
        }
        
        if convertedAmount.count > NetsQrModel.LEN_REC_TXN_AMT {
            convertedAmount = getSubstring(input: convertedAmount,
                                           start: convertedAmount.count - NetsQrModel.LEN_REC_TXN_AMT,
                                           end: convertedAmount.count)
        } else if convertedAmount.count < NetsQrModel.LEN_REC_TXN_AMT {
            let missingLength = NetsQrModel.LEN_REC_TXN_AMT - convertedAmount.count
            convertedAmount = getSubstring(input: "00000000",
                                           start: 0,
                                           end: missingLength) + convertedAmount
        }
        
        return convertedAmount;
    }
    
    
    
    
    //Converts SGQR String -> Map of SGQR Objects -> SGQR Model -> NETS QR Model
    public static func convertSgQrStringToNetsQrModel(qrData: String) throws -> NetsQrModel {
        //Get the SGQR Model from String
        let sgQrModel = parseSgQrString(qrData: qrData);
        
        //If NETS Merchant info is Present and Valid, proceed with translation
        do{
//            if let temp = sgQrModel.merchantAcctInfoSg[SgQrConsts.MerchantInfo.NETS.MERCHANT_ACCOUNT_ID],
            if let temp = findNetsMerchantAcctInfo(sgMerchantInfoMap: sgQrModel.merchantAcctInfoSg),
                try isValidSgQrNetsMerchantInfo(merchantInfo: temp){
                return try convertSgQrModelToNetsQrModel(sgQrModel: sgQrModel);
            }
        } catch let error as QrConversionError {
            throw error; //TODO check if this is ok
        }
        throw QrConversionError(code: "0003 0001",
                                message: "SGQRString->NETSQRModel: Error getting merchant account info for NETS");
    }
    
    //Top level conversion function
    //SGQR String -> Map SGQR Objects -> Generate SGQR Model -> NETS QR Model -> NETS QR String
    public static func convertSgQrToNetsQr(qrData: String) throws -> String? {
        
        do{
            //Check if SGQR is valid and proceed with conversion
            let isQrIsValid = try isValidSgQrString(qrData: qrData);
            
            if isQrIsValid{
                let netsQr = try convertSgQrStringToNetsQrModel(qrData: qrData);
                return netsQr.toString();
            }
        } catch let error as QrConversionError {
            throw error;
        }
        throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CONV_SGQRS_TO_NETSQRS,
                                                      detail: ErrorCodes.DET_SGQRS_TO_NQRS_ERR),
                                message: "SGQR->NETSQR: Error in conversion, QR is not valid");
    }
    
    public static func parseNetsQr(qrData: String) throws -> NetsQrModel {
        let builder = NetsQrModel.Builder().signature(input: "");
        let model: NetsQrModel;
        var index = 0;
        if qrData.count < 65 {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_LEN_INVALID),
                                    message: "Could not parse Nets QR: Length invalid");
        }
        
        
        
        let header = getSubstring(input: qrData,
                                  index: &index,
                                  incrementBy: NetsQrModel.LEN_HEADER);
        if header != NetsQrModel.HEADER {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_HEADER_MISMATCH),
                                    message: "Could not parse Nets QR: Header Mismatch");
        }
        
        let version = getSubstring(input: qrData,
                                   index: &index,
                                   incrementBy: NetsQrModel.LEN_VERSION);
        if version != "1" && version != "0" {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_INVALID_VERSION),
                                    message: "Could not parse Nets QR: Version Mismatch - \(version)");
        }
        
        guard let numRecords = Int(getSubstring(input: qrData,
                                                index: &index,
                                                incrementBy: NetsQrModel.LEN_NR)) else {
                                                    throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                                                                  detail: ErrorCodes.DET_NQR_FORMAT_INVALID),
                                                                            message: "Could not parse Nets QR: Could not parse Num Records");
        }
        
        let qrIssuer = getSubstring(input: qrData,
                                    index: &index,
                                    incrementBy: NetsQrModel.LEN_QR_ISSUER);
        
        let qrExpDate = getSubstring(input: qrData,
                                     index: &index,
                                     incrementBy: NetsQrModel.LEN_QR_EXP_DATE);
        
        
        _ = builder.header(input: header)
            .version(input: version)
            .qrIssuer(input: qrIssuer)
            .qrExpDate(input: qrExpDate);
        
        for _ in 0 ..< numRecords {
            guard let id = Int(getSubstring(input: qrData,
                                            index: &index,
                                            incrementBy: 2)) else {
                                                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                                                              detail: ErrorCodes.DET_NQR_FORMAT_INVALID),
                                                                        message: "Could not parse Nets QR: Could not parse Record ID at index \(index - 2)");
            }
            guard let len = Int(getSubstring(input: qrData,
                                             index: &index,
                                             incrementBy: 2)) else {
                                                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                                                              detail: ErrorCodes.DET_NQR_FORMAT_INVALID),
                                                                        message: "Could not parse Nets QR: Could not parse Record Length at index \(index - 2)");
            };
            
            let data = getSubstring(input: qrData,
                                    index: &index,
                                    incrementBy: len);
            
            _ = builder.addRecord(id: id, data: data);
        }
        
        let signature = getSubstring(input: qrData, start: index, end: qrData.count);
        
        if signature.count != NetsQrModel.LEN_SIG_V0 && signature.count != NetsQrModel.LEN_SIG_V1 {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_INVALID_SIG),
                                    message: "Could not parse Nets QR: Signature Mismatch");
        } else {
            _ = builder.signature(input: signature);
        }
        
        model = builder.build();
        if model.getMid() == nil {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_FORMAT_INVALID),
                                    message: "Could not parse Nets QR: No Mid Found");
        }
        if let merchantName = model.getMerchantName(), !merchantName.isEmpty && merchantName.count > NetsQrModel.LEN_REC_MERCHANT_NAME {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_INVALID_MERCH_NAME),
                                    message: "Could not parse Nets QR: Merchant Name Missing/Too long");
        }
        if let txnRef = model.getTxnRefNumber(), txnRef.count > NetsQrModel.LEN_REC_RETRIEVAL_REF || !stringIsAlphaNum(input: txnRef){
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETSQR,
                                                          detail: ErrorCodes.DET_NQR_INVALID_TXN_REF),
                                    message: "Txn Ref is invalid");
        }
        
        return model;
    }
    

    
    public static func parseNetsDynQrString(qrData: String) throws -> NetsDynQrModel{
        let model = NetsDynQrModel();
        var index = 0;
        
        if qrData.count < NetsDynQrModel.MIN_LENGTH {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                          detail: ErrorCodes.DET_NDYNQR_INVALID_LEN),
                                    message: "Length is too short");
        }
        
        for i in 0 ..< NetsDynQrModel.LENGTHS.count {
            let data = getSubstring(input: qrData,
                                    index: &index,
                                    incrementBy: NetsDynQrModel.LENGTHS[i]);
            
            if data.isEmpty {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                              detail: ErrorCodes.DET_NDYNQR_FORMAT_INVALID),
                                        message: "Could not parse dynamic qr, format is invalid at index: \(index - NetsDynQrModel.LENGTHS[i]) to \(index), field no: \(i)");
            }
            
            switch i {
            case 0:
                if data != NetsDynQrModel.HEADER {
                    throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                                  detail: ErrorCodes.DET_NDYNQR_INVALID_HEADER),
                                            message: "Header does not match Nets Dynamic QR Header");
                }
                model.header = data;
            case 1:
                model.version = data;
            case 2:
                model.tid = data.replacingOccurrences(of: "#", with: "");
            case 3:
                model.mid = data.replacingOccurrences(of: "#", with: "");
            case 4:
                model.stan = data;
            case 5:
                model.timeStamp = data;
            case 6:
                model.op = data;
            case 7:
                model.channel = data;
            case 8:
                model.amt = data;
            case 9:
                var b = hasAdditionalSofBlocks(bitmap: data);
                var tempData = "";
                while b {
                    let chainedData = getSubstring(input: qrData,
                                                   index: &index,
                                                   incrementBy: 1);
                    b = hasAdditionalSofBlocks(bitmap: chainedData);
                    tempData.append(chainedData);
                }
                model.paymentAccept = data;
            case 10:
                model.currency = data;
            case 11:
                model.otrs = data;
            case 12:
                model.merchantName = data;
            case 13:
                model.checksum = data;
            default:
                break;
            }
        }
        return model;
    }
    
    static func hasAdditionalSofBlocks(bitmap: String?) -> Bool{
        guard let bitmap = bitmap, !bitmap.isEmpty else {
            return false;
        }
        let length = bitmap.count;
        if length > 4{
            return false;
        } else if (length == 4){
            return hexToBytes(hex: bitmap)[0] & 0b01000000 != 0;
        } else if (length == 1){
            return hexToBytes(hex: bitmap + "0")[0] & 0b10000000 != 0;
        }
        return false;
    }
    
    static func hexToBytes(hex: String) -> [UInt8]{
        let hexa = Array(hex);
        return stride(from: 0, to: hex.count, by: 2)
            .flatMap { UInt8(String(hexa[$0..<$0.advanced(by: 2)]), radix: 16) }
    }
    
    
    //Check if is valid NETS merchant info
    public static func isValidSgQrNetsMerchantInfo(merchantInfo: SgQrObject) throws -> Bool{
        
        //Check declared ID == NETS ID
//        let netsID = String(format: "%02d", SgQrConsts.MerchantInfo.ID_NETS);
//        let isIdValid = merchantInfo.id.caseInsensitiveCompare(netsID) == .orderedSame;
//        if !isIdValid {
//            throw QrConversionError(code: "0001 0001",
//                                    message: "isValid SGQR NETS Merchant Info: Declared ID != NETS ID");
//        }
        
        //Check declared Length == NETS Length DEPRECATED
        //        let netsLength = String(format: "%02d", SgQrConsts.MerchantInfo.NETS.DATA_LENGTH);
        //        let isLenValid = merchantInfo.length.caseInsensitiveCompare(netsLength) == .orderedSame;
        //        if !isLenValid {
        //            throw QrConversionError(code: "0001 0002",
        //                                    message: "isValid SGQR NETS Merchant Info: Declared length != NETS Length");
        //        }
        
        //Get the SGQR objects out of the NETS Merchant Info Template
        let objects: [Int: SgQrObject] = extractSgQrObjectMap(merchantInfo.data);
        
        //Check NETS UNIQUE IDENTIFIER is present and Declared length matches Actual length
        if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_UNIQUE_ID],
            temp.lengthMatchesData() &&
                temp.data.compare(SgQrConsts.MerchantInfo.NETS.UNIQUE_IDENTIFIER) == .orderedSame{
            
            //Check QR METADATA declared length matches actual and expected length
            if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_QR_METADATA],
                temp.lengthMatchesData() &&
                    SgQrConsts.MerchantInfo.NETS.LEN_QR_METADATA.isInRange(temp.data.count){
                
                //Check SIGNATURE declared length matches actual and expected length
                if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_SIGNATURE],
                    temp.lengthMatchesData() &&
                        SgQrConsts.MerchantInfo.NETS.LEN_SIGNATURE.isInRange(temp.data.count){
                    
                    //Check Optional items(MERCHANT ID, TERMINAL ID, BLE METADATA)
                    if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_MERCHANT_ID],
                        !temp.lengthMatchesData() || !SgQrConsts.MerchantInfo.NETS.LEN_MERCHANT_ID.isInRange(temp.data.count){
                        throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                      detail: ErrorCodes.DET_MID_INVALID),
                                                message: "isValid SGQR NETS Merchant Info: Error in Merchant ID");
                    }
                    if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_TERMINAL_ID],
                        !temp.lengthMatchesData() || !SgQrConsts.MerchantInfo.NETS.LEN_TERMINAL_ID.isInRange(temp.data.count){
                        throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                      detail: ErrorCodes.DET_TID_INVALID),
                            message: "isValid SGQR NETS Merchant Info: Error in Terminal ID");
                    }
                    if let temp = objects[SgQrConsts.MerchantInfo.NETS.ID_BLE_METADATA],
                        !temp.lengthMatchesData() || !SgQrConsts.MerchantInfo.NETS.LEN_BLE_METADATA.isInRange(temp.data.count){
                        throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                      detail: ErrorCodes.DET_BLE_METADATA_INVALID),
                            message: "isValid SGQR NETS Merchant Info: Error in BLE Metadata");
                    }
                    return true;
                } else {
                    throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                  detail: ErrorCodes.DET_SIGNATURE_ERROR),
                        message: "isValid SGQR NETS Merchant Info: Error in Signature");
                }
            } else {
                throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                              detail: ErrorCodes.DET_INVALID_QR_METADATA),
                    message: "isValid SGQR NETS Merchant Info: Error in QR Metadata");
            }
        } else {
            throw QrConversionError(code: ErrorCodes.make(category: ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                          detail: ErrorCodes.DET_WRONG_UNIQUE_ID),
                message: "isValid SGQR NETS Merchant Info: Error in Unique identifier");
        }
    }
    
    public static func findNetsMerchantAcctInfo(sgMerchantInfoMap: [Int: SgQrObject]) -> SgQrObject? {
        var merchantDataObj: SgQrObject?
        
        let targetSubId = String(format: "%02d", SgQrConsts.MerchantInfo.NETS.ID_UNIQUE_ID)
        let targetSubLen = String(format: "%02d", SgQrConsts.MerchantInfo.NETS.LEN_UNIQUE_ID.min)
        let uniqueId = SgQrConsts.MerchantInfo.NETS.UNIQUE_IDENTIFIER
        let targetFullUniqueId = targetSubId + targetSubLen + uniqueId
        
        for entry in sgMerchantInfoMap {
            let info = entry.value
            let len = Int(info.length)
            let data = info.data
            
            if let lenNonOptional = len {
                if lenNonOptional >= 15 && data.starts(with: targetFullUniqueId){
                    merchantDataObj = info
                    break
                }
            }
        }
        return merchantDataObj
    }
    
    //Initialise list of mandatory fields
    private static func initMandatoryFields(list: inout [Int]){
        list.append(SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR);
        //        list.append(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV);
        //        list.append(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG);
        list.append(SgQrConsts.ID_SGQR_ID_INFORMATION);
        list.append(SgQrConsts.ID_MERCHANT_CATEGORY_CODE);
        list.append(SgQrConsts.ID_TRANSACTION_CURRENCY);
        list.append(SgQrConsts.ID_COUNTRY_CODE);
        list.append(SgQrConsts.ID_MERCHANT_NAME);
        list.append(SgQrConsts.ID_MERCHANT_CITY);
        list.append(SgQrConsts.ID_CRC);
    }
    
    //Create map of SGQR Object ID to Expected Length for checking
    private static func getIdToLengthMap() -> [Int: IntRange] {
        var idMap = [Int: IntRange]();
        idMap.updateValue(SgQrConsts.LEN_PAYLOAD_FORMAT_INDICATOR           , forKey: SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR);
        idMap.updateValue(SgQrConsts.LEN_POINT_OF_INITIATION_METHOD         , forKey: SgQrConsts.ID_POINT_OF_INITIATION_METHOD);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_ACCOUNT_INFORMATION_EMV   , forKey: SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_ACCOUNT_INFORMATION_SG    , forKey: SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG);
        idMap.updateValue(SgQrConsts.LEN_SGQR_ID_INFORMATION                , forKey: SgQrConsts.ID_SGQR_ID_INFORMATION);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_CATEGORY_CODE             , forKey: SgQrConsts.ID_MERCHANT_CATEGORY_CODE);
        idMap.updateValue(SgQrConsts.LEN_TRANSACTION_CURRENCY               , forKey: SgQrConsts.ID_TRANSACTION_CURRENCY);
        idMap.updateValue(SgQrConsts.LEN_TRANSACTION_AMOUNT                 , forKey: SgQrConsts.ID_TRANSACTION_AMOUNT);
        idMap.updateValue(SgQrConsts.LEN_TIP_OR_CONVENIENCE_INDICATOR       , forKey: SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR);
        idMap.updateValue(SgQrConsts.LEN_VALUE_OF_CONVENIENCE_FEE_FIXED     , forKey: SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED);
        idMap.updateValue(SgQrConsts.LEN_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE, forKey: SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE);
        idMap.updateValue(SgQrConsts.LEN_COUNTRY_CODE                       , forKey: SgQrConsts.ID_COUNTRY_CODE);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_NAME                      , forKey: SgQrConsts.ID_MERCHANT_NAME);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_CITY                      , forKey: SgQrConsts.ID_MERCHANT_CITY);
        idMap.updateValue(SgQrConsts.LEN_POSTAL_CODE                        , forKey: SgQrConsts.ID_POSTAL_CODE);
        idMap.updateValue(SgQrConsts.LEN_ADDITIONAL_TEMPLATE_DATA           , forKey: SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA);
        idMap.updateValue(SgQrConsts.LEN_MERCHANT_INFO_LANGUAGE_TEMPLATE    , forKey: SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE);
        idMap.updateValue(SgQrConsts.LEN_RFU_FOR_EMV                        , forKey: SgQrConsts.ID_RFU_FOR_EMV);
        idMap.updateValue(SgQrConsts.LEN_UNRESERVED_TEMPLATES               , forKey: SgQrConsts.ID_UNRESERVED_TEMPLATES);
        idMap.updateValue(SgQrConsts.LEN_CRC                                , forKey: SgQrConsts.ID_CRC);
        return idMap;
    }
    
    //Check if certain IDs are present
    public static func checkMandatoryFieldsExist(mandatoryFields: [Int], foundFields: [Int: Bool]) -> Bool {
        //Return false if any mandatory field cant be found
        for field in mandatoryFields {
            if !foundFields.keys.contains(field){
                return false;
            }
        }
        return true;
    }
    
    //Check if a length matches an ID
    public static func isLengthValid(key: Int, length: Int, idMap: [Int: IntRange]) -> Bool {
        
        //Return false if key is out of range
        if key < 0 || key > SgQrConsts.MAX_ID_VAL {
            return false;
        } else {
            
            //If Object ID is in the map, check if length matches
            if idMap.keys.contains(key){
                let entry = idMap[key];
                return entry!.min <= length && length <= entry!.max;
            } else {
                //If object ID is not in the map(Such as when it is in a group range,
                //e.g. ID = 35 is in Group with IDs 26-50, but only 26 is in the map
                var i = key;
                while(!idMap.keys.contains(i) && i >= 0){
                    i -= 1;
                }
                //If reached 0, then it means item does not exist somehow?
                if i < 0 {
                    return false;
                } else {
                    let entry = idMap[i];
                    return entry!.min <= length && length <= entry!.max;
                }
            }
        }
    }
    
    //Function to get substring, dont want to extend String
    //in case user already implemented their own extension with same name
    public static func getSubstring(input inputString: String, start: Int, end: Int) -> String {
        if(inputString.isEmpty ||
            start >= end ||
            start < 0 ||
            end > inputString.count
            ){
            return "";
        }
        let startIndex   = inputString.index(inputString.startIndex, offsetBy: start);
        let endIndex     = inputString.index(inputString.startIndex, offsetBy: end);
        let subString    = inputString[startIndex ..< endIndex];
        
        return String(subString);
    }
    
    
    public static func getSubstring(input inputString: String, index: inout Int, incrementBy: Int) -> String {
        if (inputString.isEmpty ||
            index < 0 ||
            index + incrementBy > inputString.count
            ){
            return "";
        }
        let startIndex   = inputString.index(inputString.startIndex, offsetBy: index);
        let endIndex     = inputString.index(inputString.startIndex, offsetBy: index + incrementBy);
        let subString    = inputString[startIndex ..< endIndex];
        
        index += incrementBy;
        return String(subString);
    }
    
    private static func stringIsAlphaNum(input: String) -> Bool{
        let regex = "[^a-zA-Z0-9]"
        return input.range(of: regex, options: .regularExpression, range: nil, locale: nil) == nil
    }
    
}










