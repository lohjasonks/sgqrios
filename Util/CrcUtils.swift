//
//  CrcUtils.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation

class CrcUtils{
    public static func crc16(data: [UInt8],seed: UInt16 = 0xffff, final: UInt16 = 0xffff)->UInt16{
        var crc = seed
        data.forEach { (byte) in
            crc ^= UInt16(byte) << 8
            (0..<8).forEach({ _ in
                crc = UInt16((crc & UInt16(0x8000)) != 0 ? (crc << 1) ^ 0x1021 : crc << 1)
            })
        }
        return UInt16(crc & final)
    }
}
