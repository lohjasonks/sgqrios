//
//  SgQrConsts.swift
//  QrUtility
//
//  Created by Jason Loh on 17/11/17.
//

import Foundation

struct SgQrConsts{
    static let MAX_ID_VAL = 99;
    static let MAX_LEN_VAL = 99;
    
    //Field IDs
    static let ID_PAYLOAD_FORMAT_INDICATOR = 0;
    static let ID_POINT_OF_INITIATION_METHOD = 1;
    static let ID_MERCHANT_ACCOUNT_INFORMATION_EMV = 2;
    static let ID_MERCHANT_ACCOUNT_INFORMATION_SG = 26;
    static let ID_SGQR_ID_INFORMATION = 51;
    static let ID_MERCHANT_CATEGORY_CODE = 52;
    static let ID_TRANSACTION_CURRENCY = 53;
    static let ID_TRANSACTION_AMOUNT = 54;
    static let ID_TIP_OR_CONVENIENCE_INDICATOR = 55;
    static let ID_VALUE_OF_CONVENIENCE_FEE_FIXED = 56;
    static let ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE = 57;
    static let ID_COUNTRY_CODE = 58;
    static let ID_MERCHANT_NAME = 59;
    static let ID_MERCHANT_CITY = 60;
    static let ID_POSTAL_CODE = 61;
    static let ID_ADDITIONAL_TEMPLATE_DATA = 62;
    static let ID_MERCHANT_INFO_LANGUAGE_TEMPLATE = 64;
    static let ID_RFU_FOR_EMV = 65;
    static let ID_UNRESERVED_TEMPLATES = 80;
    static let ID_CRC = 63;
    
    //Field Lengths
    static let LEN_PAYLOAD_FORMAT_INDICATOR = IntRange(2, 2);
    static let LEN_POINT_OF_INITIATION_METHOD = IntRange(2, 2);
    static let LEN_MERCHANT_ACCOUNT_INFORMATION_EMV = IntRange(5, 99);
    static let LEN_MERCHANT_ACCOUNT_INFORMATION_SG = IntRange(5, 99);
    static let LEN_SGQR_ID_INFORMATION = IntRange(1, 99);
    static let LEN_MERCHANT_CATEGORY_CODE = IntRange(4, 4);
    static let LEN_TRANSACTION_CURRENCY = IntRange(3, 3);
    static let LEN_TRANSACTION_AMOUNT = IntRange(1, 13);
    static let LEN_TIP_OR_CONVENIENCE_INDICATOR = IntRange(2, 2);
    static let LEN_VALUE_OF_CONVENIENCE_FEE_FIXED = IntRange(1, 13);
    static let LEN_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE = IntRange(1, 5);
    static let LEN_COUNTRY_CODE = IntRange(2, 2);
    static let LEN_MERCHANT_NAME = IntRange(1, 25);
    static let LEN_MERCHANT_CITY = IntRange(1, 15);
    static let LEN_POSTAL_CODE = IntRange(6, 10);
    static let LEN_ADDITIONAL_TEMPLATE_DATA = IntRange(1, 99);
    static let LEN_MERCHANT_INFO_LANGUAGE_TEMPLATE = IntRange(1, 99);
    static let LEN_RFU_FOR_EMV = IntRange(1, 99);
    static let LEN_UNRESERVED_TEMPLATES = IntRange(1, 99);
    static let LEN_CRC = IntRange(4, 4);
    
    static let NUM_MERCHANTS_EMV = 24;
    static let NUM_MERCHANTS_SG = 25;
    static let NUM_RFU_FOR_EMV = 15;
    static let NUM_UNRESERVED_TEMPLATES = 24;
    
    public struct AddnData{
        static let ID_BILL_NUMBER = 1;
        static let ID_MOBILE_NUMBER = 2;
        static let ID_STORE_LABEL = 3;
        static let ID_LOYALTY_NUMBER = 4;
        static let ID_REFERENCE_LABEL = 5;
        static let ID_CUSTOMER_LABEL = 6;
        static let ID_TERMINAL_LABEL = 7;
        static let ID_PURPOSE_OF_TXN = 8;
        static let ID_ADDN_CUST_DATA_REQ = 9;
        static let ID_RFU_FOR_EMV = 10;
        static let ID_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES = 50;
        
        static let LEN_BILL_NUMBER = IntRange(1, 25);
        static let LEN_MOBILE_NUMBER = IntRange(1, 25);
        static let LEN_STORE_LABEL = IntRange(1, 25);
        static let LEN_LOYALTY_NUMBER = IntRange(1, 25);
        static let LEN_REFERENCE_LABEL = IntRange(1, 25);
        static let LEN_CUSTOMER_LABEL = IntRange(1, 25);
        static let LEN_TERMINAL_LABEL = IntRange(1, 25);
        static let LEN_PURPOSE_OF_TXN = IntRange(1, 25);
        static let LEN_ADDN_CUST_DATA_REQ = IntRange(1, 3);
        static let LEN_RFU_FOR_EMV = IntRange(1, 99);
        static let LEN_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES = IntRange(1, 99);
    }
    
    
    public struct MerchantInfo{
        static let ID_VISA_1 = 2;
        static let ID_VISA_2 = 3;
        static let ID_MASTERCARD_1 = 4;
        static let ID_MASTERCARD_2 = 5;
        static let ID_EMVCO_1 = 6;
        static let ID_EMVCO_2 = 7;
        static let ID_EMVCO_3 = 8;
        static let ID_DISCOVER_1 = 9;
        static let ID_DISCOVER_2 = 10;
        static let ID_AMEX_1 = 11;
        static let ID_AMEX_2 = 12;
        static let ID_JCB_1 = 13;
        static let ID_JCB_2 = 14;
        static let ID_UNIONPAY_1 = 15;
        static let ID_UNIONPAY_2 = 16;
        static let ID_EMVCO_4 = 17;
        static let ID_EMVCO_5 = 18;
        static let ID_EMVCO_6 = 19;
        static let ID_EVCO_7 = 20;
        static let ID_EMVCO_8 = 21;
        static let ID_EMVCO_9 = 22;
        static let ID_EMVCO_10 = 23;
        static let ID_EMVCO_11 = 24;
        static let ID_EMVCO_12 = 25;
        
        static let ID_SINGTEL_DASH = 26;
        static let ID_LIQUID_PAY = 27;
        static let ID_OCBC_P2P = 28;
        static let ID_EZI_WALLET = 29;
        static let ID_EZ_LINK = 30;
        static let ID_GRAB_PAY = 31;
        static let ID_DBS = 32;
        static let ID_NETS = 33;
        static let ID_WECHAT_PAY = 34;
        static let ID_UOB = 35;
        static let ID_PAYNOW = 36;
        static let ID_AIRPAY_SEA = 37;
        
        public struct NETS{
            static let MERCHANT_ACCOUNT_ID = 33;
            static let DATA_LENGTH = 85;
            
            static let UNIQUE_IDENTIFIER = "SG.COM.NETS";
            
            static let ID_UNIQUE_ID = 0;
            static let ID_QR_METADATA = 1;
            static let ID_MERCHANT_ID = 2;
            static let ID_TERMINAL_ID = 3;
            static let ID_BLE_METADATA = 4;
            static let ID_TXN_AMT_MODIFIER = 9;
            static let ID_SIGNATURE = 99;
            
            static let LEN_UNIQUE_ID = IntRange(11, 11);
            static let LEN_QR_METADATA = IntRange(23, 23);
            static let LEN_MERCHANT_ID = IntRange(1, 99);
            static let LEN_TERMINAL_ID = IntRange(1, 99);
            static let LEN_BLE_METADATA = IntRange(80, 80);
            static let LEN_TXN_AMT_MODIFIER = IntRange(1, 1);
            static let LEN_SIGNATURE = IntRange(8, 8);
        }
    }
}
