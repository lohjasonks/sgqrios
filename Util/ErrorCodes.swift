//
//  ErrorCodes.swift
//  QrUtility
//
//  Created by Jason Loh on 17/7/18.
//  Copyright © 2018 BixelTest. All rights reserved.
//

import Foundation
class ErrorCodes{
    
    
    static let CAT_CHECK_SGQR              = "0000";
    static let DET_CRC_IDLEN_INVALID       = "0001";
    static let DET_CRC_VALUE_INVALID       = "0002";
    static let DET_CANT_PARSE_IDLEN        = "0003";
    static let DET_DUPLICATE_ENTRIES       = "0004";
    static let DET_ID_LEN_MISMATCH         = "0005";
    static let DET_LEN_TOO_LONG            = "0006";
    static let DET_REMAINING_LEN_ERR       = "0007";
    static let DET_MANDATORY_FIELD_MISSING = "0008";
    
    
    static let CAT_CHECK_MERCH_INFO_NETS    = "0001";
    static let DET_MERCH_INFO_NULL          = "0000";
    static let DET_MERCH_INFO_ID_NOT_NETS   = "0001";
    static let DET_MERCH_INFO_LEN_INVALID   = "0002";
    static let DET_WRONG_UNIQUE_ID          = "0003";
    static let DET_INVALID_QR_METADATA      = "0004";
    static let DET_SIGNATURE_ERROR          = "0005";
    static let DET_MID_INVALID              = "0006";
    static let DET_TID_INVALID              = "0007";
    static let DET_BLE_METADATA_INVALID     = "0008";
    static let DET_TXN_AMT_MODIFIER_INVALID = "0009";
    static let DET_NETS_MI_GENERAL_ERR      = "0010";
    
    
    static let CAT_CONV_SGQRM_TO_NETSQRM        = "0002";
    static let DET_NETS_MI_MISSING              = "0000";
    static let DET_QR_METADATA_NULL             = "0001";
    static let DET_MID_MISSING                  = "0002";
    static let DET_MERCH_NAME_MISSING           = "0003";
    static let DET_SIG_MISSING                  = "0004";
    static let DET_TXN_REF_INVALID              = "0005";
    static let DET_UNACCEPTED_CURRENCY_CODE     = "0006";
    static let DET_AMT_INVALID_FOR_NETSQRM      = "0007";
    static let DET_SGQRM_TO_NETSQRM_GENERAL_ERR = "0010";
    
    
    static let CAT_CONV_SGQRS_TO_NETSQRM     = "0003";
    static let DET_INVALID_INFO              = "0001";
    static let DET_SGQRS_TO_NQRM_GENERAL_ERR = "0010";
    
    static let CAT_CONV_SGQRS_TO_NETSQRS = "0004";
    static let DET_SGQRS_TO_NQRS_ERR     = "0001";
    
    
    //Is not thrown in Swift, only in Java
    static let CAT_PARSE_SGQR_STRING     = "0005";
    static let DET_PARSESGQR_GENERAL_ERR = "0001";
    static let DET_AMT_INVALID           = "0002";
    
    
    //Is not thrown in Swift, Only in Java
    static let CAT_PARSE_SGQR_MAP         = "0009";
    static let DET_EXTRACTION_GENERAL_ERR = "0001";
    
    static let CAT_PARSE_NETSQR           = "0010";
    static let DET_NQR_LEN_INVALID        = "0001";
    static let DET_NQR_HEADER_MISMATCH    = "0002";
    static let DET_NQR_INVALID_VERSION    = "0003";
    static let DET_NQR_INVALID_SIG        = "0004";
    static let DET_NQR_INVALID_MERCH_NAME = "0005";
    static let DET_NQR_INVALID_TXN_REF    = "0006";
    static let DET_NQR_FORMAT_INVALID     = "0006";
    
    
    static let CAT_PARSE_NETS_DYNQR      = "0020";
    static let DET_NDYNQR_INVALID_LEN    = "0001";
    static let DET_NDYNQR_INVALID_HEADER = "0002";
    static let DET_NDYNQR_FORMAT_INVALID = "0003";
    
    public static func make(category: String, detail: String) -> String{
        return category + " " + detail
    }
    
}
