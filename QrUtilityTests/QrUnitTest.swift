//
//  QrUnitTest.swift
//  QrUtilityTests
//
//  Created by Jason Loh on 21/11/17.
//

import XCTest
@testable import QrUtility

class QrUnitTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    

    
    func testRef(){
        let model = NetsQrModel.Builder()
            .header(input: "NETSqpay")
            .version(input: "0")
            .qrIssuer(input: "0123456789")
            .qrExpDate(input: "31122018")
            .mid(input: "11135036100")
            .merchantName(input: "NETS_TST_QR_DRINKS")
            .tid(input: "35036103")
            .secretString(input: SECRET_STRING)
            .build();
    
        
        XCTAssertFalse(model.isTxnReferenceProvidedByUser());
        
        let model2 = NetsQrModel.Builder()
            .header(input: "NETSqpay")
            .version(input: "0")
            .qrIssuer(input: "0123456789")
            .qrExpDate(input: "31122018")
            .mid(input: "11135036100")
            .merchantName(input: "NETS_TST_QR_DRINKS")
            .tid(input: "35036103")
            .secretString(input: SECRET_STRING)
            .txnRefNumber(input: "123123")
            .build();
        XCTAssertFalse(model2.isTxnReferenceProvidedByUser());
        
        let model3 = NetsQrModel.Builder()
            .header(input: "NETSqpay")
            .version(input: "0")
            .qrIssuer(input: "0123456789")
            .qrExpDate(input: "31122018")
            .mid(input: "11135036100")
            .merchantName(input: "NETS_TST_QR_DRINKS")
            .tid(input: "35036103")
            .secretString(input: SECRET_STRING)
            .txnRefNumber(input: "")
            .build();
        
        XCTAssertTrue(model3.isTxnReferenceProvidedByUser());
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
    let UEN             = "0123456789";
    let SECRET_STRING   = "THISISNETSSECRETSTRINGFORTESTING";
    let MID             = "11135036100";
    let TID             = "35036103";
    let HEADER          = "NETSqpay";
    let EXPIRY          = "31122018";
    let VERSION         = "1";
    let MERCHANT_NAME   = "TEST_MERCHANT_NETS";
    let AMOUNT          = "00001234";
    let REFERENCE       = "1A2B3C4D5E6F";
    let REFERENCE_EMPTY = "";
    let QUALIFIER_FALSE = "0";
    let QUALIFIER_TRUE  = "1";
    
    func testNetsQrGenerationTest(){
        let REFERENCE_EMPTY = "";
        let QUALIFIER_FALSE = "0";
        let QUALIFIER_TRUE  = "1";
        
        let amtTrueRefTrue = NetsQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION)
            .qrIssuer(input: UEN)
            .qrExpDate(input: EXPIRY)
            .mid(input: MID)
            .merchantName(input: MERCHANT_NAME)
            .tid(input: TID)
            .txnAmout(input: AMOUNT)
            .txnRefNumber(input: REFERENCE)
            .secretString(input: SECRET_STRING)
            .txnAmountQualifier(input: QUALIFIER_FALSE)
            .build();
        
        let amtTrueRef0 = NetsQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION)
            .qrIssuer(input: UEN)
            .qrExpDate(input: EXPIRY)
            .mid(input: MID)
            .merchantName(input: MERCHANT_NAME)
            .tid(input: TID)
            .txnAmout(input: AMOUNT)
            .txnRefNumber(input: REFERENCE_EMPTY)
            .secretString(input: SECRET_STRING)
            .txnAmountQualifier(input: QUALIFIER_FALSE)
            .build();
        
        let amtTrueRefTrueQualifierTrue = NetsQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION)
            .qrIssuer(input: UEN)
            .qrExpDate(input: EXPIRY)
            .mid(input: MID)
            .merchantName(input: MERCHANT_NAME)
            .tid(input: TID)
            .txnAmout(input: AMOUNT)
            .txnRefNumber(input: REFERENCE)
            .secretString(input: SECRET_STRING)
            .txnAmountQualifier(input: QUALIFIER_TRUE)
            .build();
        
        
        print(amtTrueRefTrue.toString());
        print(amtTrueRef0.toString());
        print(amtTrueRefTrueQualifierTrue.toString());
        print("asd");
        
    }
    
    func testParseNetsQr(){
        let netsQrString = HEADER + VERSION + "06" + UEN + EXPIRY +
            "0011" + MID +
            "0116" + "TESTMERCHANTNETS" +
            "0208" + TID +
            "0308" + AMOUNT +
            "0712" + REFERENCE +
            "0901" + "0" + "d83e3f50";
        
        print(netsQrString);
        var model: NetsQrModel = NetsQrModel();
        do {
            model =  try QrUtils.parseNetsQr(qrData: netsQrString);
        } catch let err as QrConversionError {
            print(err.getInfo());
        } catch let err{
            print(err);
            XCTFail();
        }
        
        
        XCTAssertEqual(model.getHeader(), HEADER);
        XCTAssertEqual(model.getVersion(), VERSION);
        XCTAssertEqual(model.getNr(), "06");
        XCTAssertEqual(model.getQrIssuer(), UEN);
        XCTAssertEqual(model.getQrExpDate(), EXPIRY);
        XCTAssertEqual(model.getMid(), MID);
        XCTAssertEqual(model.getTid(), TID);
        XCTAssertEqual(model.getMerchantName(), "TESTMERCHANTNETS");
        XCTAssertEqual(model.getTxnAmount(), AMOUNT);
        XCTAssertEqual(model.getTxnRefNumber(), REFERENCE);
        XCTAssertEqual(model.getSignature(), "d83e3f5000000000000000000000000000000000000000000000000000000000");
    }
//    class HasMembers{
//        var emptyString: String;
//        var nonEmptyString: String;
//
//        init(){
//            emptyString = "";
//            nonEmptyString = "";
//        }
//    }
//
//    func testSomeTest(){
//        var hasMembers: HasMembers?;
//        hasMembers = HasMembers();
//        let notEmptyString = "Not Empty";
//        hasMembers?.nonEmptyString = notEmptyString;
//
//        var someString: String;
//        someString = hasMembers?.nonEmptyString;
//        doSomething(input: hasMembers?.nonEmptyString);
//        hasMembers = nil;
//        doSomething(input: hasMembers?.emptyString);
//
//
//    }
//
//    func doSomething(input: String?){
//        if let input = input {
//            print(input + "ADADSAD");
//        } else {
//            print("Was empty");
//        }
//    }
    
}
