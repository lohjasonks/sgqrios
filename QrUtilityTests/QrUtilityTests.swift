//
//  QrUtilityTests.swift
//  QrUtilityTests
//
//  Created by Jason Loh on 17/11/17.
//

import XCTest
@testable import QrUtility	

class QrUtilityTests: XCTestCase {
    
    let dataFull = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE61060810066304F175";

    let dataLessCrc = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE6106081006";

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCrc() {
        XCTAssertTrue(QrUtils.crcIsValid(qrData: dataFull));
        XCTAssertFalse(QrUtils.crcIsValid(qrData: dataLessCrc));
    }
    
    func testIsValidSgQr() {
        do{
            XCTAssertTrue(try QrUtils.isValidSgQrString(qrData: dataFull));
        }
        XCTAssertThrowsError(try QrUtils.isValidSgQrString(qrData: dataLessCrc));
        XCTAssertThrowsError(try QrUtils.isValidSgQrString(qrData: "000110102030203333"));
    }
    
    func testExtractSgQrMap(){
        var map : [Int: SgQrObject]? = nil;
        map = QrUtils.extractSgQrObjectMap(dataFull);
        XCTAssertNotNil(map);
        let umap = map!;
        for (_, object) in umap {
            XCTAssertNotNil(object);
            print(object.toString());
            XCTAssertTrue(!object.length.isEmpty);
            XCTAssertTrue(!object.id.isEmpty);
            XCTAssertTrue(!object.data.isEmpty);
            XCTAssertEqual(Int(object.length), object.data.count);
            XCTAssertEqual(umap.count, 26);
        }
    }
    

    func testAmountWithDecimalSgqrToNetsQr() {
        do {
            let sgqrString = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF52040000530370254071234.565802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363047191";
            let _ = try QrUtils.convertSgQrStringToNetsQrModel(qrData: sgqrString);
    
    
            let emptyAmtSgqr = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF5204000053037025802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363047ae3";
            let freeAmtSgqr = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF520400005303702540105802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363044199";
    
            let _ = try QrUtils.convertSgQrStringToNetsQrModel(qrData: emptyAmtSgqr);
            let _ = try QrUtils.convertSgQrStringToNetsQrModel(qrData: freeAmtSgqr);
    
            let invalidTooManyDecimalsSgqr = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF520400005303702540612.3455802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363049b3b";
            let _ = try QrUtils.convertSgQrStringToNetsQrModel(qrData: invalidTooManyDecimalsSgqr);
            XCTFail();
            } catch let err as QrConversionError {
            print(err.errorCode);
            print(err.errorMessage);
        }catch {
            print("General Error");
            XCTFail();
        }
    }
    
    func testConversion14Feb(){
        let sgqr = "00020101021133860011SG.COM.NETS012310123456789181231235959021111135036100030835036103090109908A08F2D8D51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE630442f5"
        
        do {
            let _ = try QrUtils.convertSgQrToNetsQr(qrData: sgqr);
        } catch let conversionErr as QrConversionError{
            print(conversionErr.errorCode);
            print(conversionErr.errorMessage);
            XCTFail();
        } catch {
            print("General Error");
            XCTFail();
        }
    }
    
    func test15Feb(){
        let qr = "00020101021133860011SG.COM.NETS012310123456789181231235959021111135036100030835036103090109908A08F2D8D51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE610688833362070103***64270002EN0117NAMEINALTLANGUAGE63049f1a"
        
        let qr2 = "00020101021133860011SG.COM.NETS01231012345678918123123595902111113503610003083503610309011990810E602FC51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE6304eebb";
        
        var netsqr: NetsQrModel?;
        var netsqr2: NetsQrModel?;
        do {
            netsqr = try QrUtils.convertSgQrStringToNetsQrModel(qrData: qr);
            netsqr2 = try QrUtils.convertSgQrStringToNetsQrModel(qrData: qr2);
        } catch let err as QrConversionError {
            print(err.errorCode);
            print(err.errorMessage);
        } catch {
            print("nonQRCONVERSIONERROR");
        }
        guard let _netsqr = netsqr, let _netsqr2 = netsqr2 else {
            XCTFail();
            return;
        }
        XCTAssertTrue(_netsqr.isTxnReferenceProvidedByUser());
        XCTAssertEqual(_netsqr.getTxnRefNumber(),"");
        
        
        XCTAssertFalse(_netsqr2.isTxnReferenceProvidedByUser());
        print(netsqr2?.getTxnRefNumber());
        
        
    }
    
    
    func testIsValidSgQrNetsMerchantInfo(){
        let sgQr = QrUtils.parseSgQrString(qrData: dataFull);
        let obj = sgQr.merchantAcctInfoSg[SgQrConsts.MerchantInfo.ID_NETS];
        do {
            let isValidMerchantInfo = try QrUtils.isValidSgQrNetsMerchantInfo(merchantInfo: obj!);
            XCTAssertTrue(isValidMerchantInfo)
        } catch {
            XCTFail();
        }
        obj!.id = "35";
        XCTAssertTrue(try QrUtils.isValidSgQrNetsMerchantInfo(merchantInfo: obj!));
    }
    
    func testConvertSgQrStringToModel(){
        let sgQr = QrUtils.parseSgQrString(qrData: dataFull);
        XCTAssertEqual(sgQr.payloadFormatIndicator, "01");
        XCTAssertEqual(sgQr.pointOfInitiationMethod, "11");
        XCTAssertEqual(sgQr.sgQrIdentityInformation, "0007SG.SGQR01112017123456X0209S1234567A");
        XCTAssertEqual(sgQr.merchantCategoryCode, "5814");
        XCTAssertEqual(sgQr.transactionCurrency, "702");
        XCTAssertEqual(sgQr.merchantCountryCode, "SG");
        XCTAssertEqual(sgQr.merchantName, "FOOD XYZ PTE LTD");
        XCTAssertEqual(sgQr.merchantCity, "SINGAPORE");
        XCTAssertEqual(sgQr.postalCode, "081006");
        XCTAssertEqual(sgQr.crc, "F175");
        
        for (_, object) in sgQr.merchantAcctInfoEmv {
            XCTAssertNotNil(object);
            print(object.toString());
            XCTAssertTrue(!object.length.isEmpty);
            XCTAssertTrue(!object.id.isEmpty);
            XCTAssertTrue(!object.data.isEmpty);
            XCTAssertEqual(Int(object.length), object.data.count);
        }
        for (_, object) in sgQr.merchantAcctInfoSg {
            XCTAssertNotNil(object);
            print(object.toString());
            XCTAssertTrue(!object.length.isEmpty);
            XCTAssertTrue(!object.id.isEmpty);
            XCTAssertTrue(!object.data.isEmpty);
            XCTAssertEqual(Int(object.length), object.data.count);
        }
        for (_, object) in sgQr.additionalData {
            XCTAssertNotNil(object);
            print(object.toString());
            XCTAssertTrue(!object.length.isEmpty);
            XCTAssertTrue(!object.id.isEmpty);
            XCTAssertTrue(!object.data.isEmpty);
            XCTAssertEqual(Int(object.length), object.data.count);
        }
        for (_, object) in sgQr.merchantInformationLanguageTemplate {
            XCTAssertNotNil(object);
            print(object.toString());
            XCTAssertTrue(!object.length.isEmpty);
            XCTAssertTrue(!object.id.isEmpty);
            XCTAssertTrue(!object.data.isEmpty);
            XCTAssertEqual(Int(object.length), object.data.count);
        }
        
    }
    
    func testConvertSgQrModelToNetsQrModel(){
        do {
            let netsQr = try QrUtils.convertSgQrModelToNetsQrModel(sgQrModel: QrUtils.parseSgQrString(qrData: dataFull));
            XCTAssertNotNil(netsQr);
            XCTAssertEqual(netsQr.header, NetsQrModel.HEADER);
            XCTAssertEqual(netsQr.version, "1");
            XCTAssertEqual(netsQr.qrIssuer, "2014018328");
            XCTAssertEqual(netsQr.qrExpDate, "88122031");
            for record in netsQr.records {
                XCTAssertNotNil(record);
                XCTAssertTrue(!record.length.isEmpty);
                XCTAssertTrue(!record.type.isEmpty);
                XCTAssertTrue(!record.data.isEmpty);
                XCTAssertEqual(Int(record.length), record.data.count);
            }
            XCTAssertEqual(netsQr.signature, "4E5DC3D800000000000000000000000000000000000000000000000000000000");
        } catch {
            XCTFail();
        }

    }

    func testConvertSgQrStringToNetsQrModel(){
        do {
            let netsQr = try QrUtils.convertSgQrStringToNetsQrModel(qrData: dataFull);
            XCTAssertNotNil(netsQr);
            XCTAssertEqual(netsQr.header, NetsQrModel.HEADER);
            XCTAssertEqual(netsQr.version, "1");
            XCTAssertEqual(netsQr.qrIssuer, "2014018328");
            XCTAssertEqual(netsQr.qrExpDate, "88122031");
            for record in netsQr.records {
                XCTAssertNotNil(record);
                XCTAssertTrue(!record.length.isEmpty);
                XCTAssertTrue(!record.type.isEmpty);
                XCTAssertTrue(!record.data.isEmpty);
                XCTAssertEqual(Int(record.length), record.data.count);
            }
            XCTAssertEqual(netsQr.signature, "4E5DC3D800000000000000000000000000000000000000000000000000000000");
        } catch {
            XCTFail();
        }

    }
    
    func testConvertSgQrToNetsQr () {
        
        let shoudBe = "NETSqpay10320140183288812203100150001118703240000116FOOD XYZ PTE LTD0208885872014E5DC3D800000000000000000000000000000000000000000000000000000000";
        do{
            let output = try QrUtils.convertSgQrToNetsQr(qrData: dataFull);
            XCTAssertEqual(output, shoudBe);
        } catch {
            XCTFail();
        }
        XCTAssertThrowsError(try QrUtils.convertSgQrToNetsQr(qrData: dataLessCrc));
        
    }
    
    func testSgQrToString(){
        let sgQr = QrUtils.parseSgQrString(qrData: dataFull);
        let newSgQr = sgQr.toString();
        XCTAssertEqual(newSgQr, dataFull);
    }
    
    func testSubstringer(){
        let testString = "0123456789";
        let length = testString.count;
        let zeroToFive = QrUtils.getSubstring(input: testString, start: 0, end: 5);
        let fiveToNine = QrUtils.getSubstring(input: testString, start: 5, end: 9);
        let four = QrUtils.getSubstring(input: testString, start: 4, end: 5);
        let startTooSmall = QrUtils.getSubstring(input: testString, start: -1, end: 0);
        let endTooBig = QrUtils.getSubstring(input: testString, start: 0, end: testString.count + 1);
        let startBiggerThanEnd = QrUtils.getSubstring(input: testString, start: 6, end: 5);
        let emptyString = QrUtils.getSubstring(input: "", start: 0, end: 0);
        let fullString = QrUtils.getSubstring(input: testString, start: 0, end: testString.count);
        let fromEnd = QrUtils.getSubstring(input: testString, start: length - 8, end: length - 4);
        
        
        XCTAssertEqual(zeroToFive, "01234");
        XCTAssertEqual(fiveToNine, "5678");
        XCTAssertEqual(four, "4");
        XCTAssertEqual(startTooSmall, "");
        XCTAssertEqual(endTooBig, "");
        XCTAssertEqual(startBiggerThanEnd, "");
        XCTAssertEqual(emptyString, "");
        XCTAssertEqual(fullString, testString);
        XCTAssertEqual(fromEnd, "2345")
    }
    
    
    func testHasAddtionalSofBlocks(){
        let hexToBytes = {(hex: String) -> [UInt8] in
            let hexa = Array(hex);
            return stride(from: 0, to: hex.count, by: 2)
                .flatMap { UInt8(String(hexa[$0..<$0.advanced(by: 2)]), radix: 16) }
        }
        
        let hasAdditionalSofBlocks = { (bitmap: String?) -> Bool in
            guard let bitmap = bitmap, !bitmap.isEmpty else {
                return false;
            }
            let length = bitmap.count;
            if length > 4{
                return false;
            } else if (length == 4){
                return hexToBytes(bitmap)[0] & 0b01000000 != 0;
            } else if (length == 1){
                return hexToBytes(bitmap + "0")[0] & 0b10000000 != 0;
            }
            return false;
        }
    
        

        XCTAssertTrue(hasAdditionalSofBlocks("4000"));
        XCTAssertFalse(hasAdditionalSofBlocks("8000"));
        XCTAssertTrue(hasAdditionalSofBlocks("8"));
        XCTAssertFalse(hasAdditionalSofBlocks("4"));
        XCTAssertFalse(hasAdditionalSofBlocks("800000"));
        XCTAssertFalse(hasAdditionalSofBlocks("400000"));
        

        
    }
    
    func testParseNetsQr() throws {
        let UEN             = "0123456789";
        let SECRET_STRING   = "THISISNETSSECRETSTRINGFORTESTING";
        let MID             = "11135036100";
        let TID             = "35036103";
        let HEADER          = "NETSqpay";
        let EXPIRY          = "31122032";
        let WEBQR_TRUE      = "1";
        let WEBQR_FALSE     = "0";
        let VERSION1        = "1";
        let VERSION0        = "0";
        let MERCHANT_NAME   = "TEST_MERCHANT_NETS";
        let AMOUNT          = "00001234";
        let REFERENCE       = "1A2B3C4D5E6F";
        let REFERENCE_EMPTY = "";
        let QUALIFIER_FALSE = "0";
        let QUALIFIER_TRUE  = "1";
        let TIMESTAMP       = "02042018083015";
        let MINMAX          = "0000000199999999";
        
        var builder = NetsQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION0)
            .qrIssuer(input: UEN)
            .txnTimestamp(input: TIMESTAMP)
            .mid(input: MID)
            .merchantName(input: MERCHANT_NAME)
            .tid(input: TID)
            .txnAmout(input: AMOUNT)
            .txnAmountQualifier(input: QUALIFIER_TRUE)
            .qrExpDate(input: EXPIRY)
            .secretString(input: SECRET_STRING)
            .webQr(input: WEBQR_TRUE);
        
        let webQrModel = builder.build();
        let netsQrWithWebQr = builder.build().toString();
        print("WebQrNetsQr:\n" + netsQrWithWebQr);


        let parsedModel = try QrUtils.parseNetsQr(qrData: netsQrWithWebQr);
        XCTAssertEqual(webQrModel.toString(), parsedModel.toString())
        XCTAssertEqual(webQrModel.getWebQr(), parsedModel.getWebQr())
        XCTAssertTrue(parsedModel.isWebQr())
        XCTAssertEqual(parsedModel.getRecordData(id: 10), webQrModel.getRecordData(id: 10))
    }
    
    func testParseDyn() throws {
        let HEADER = "NETSQPAY";
        let VERSION = "0";
        let TID = "10030028";
        let TID_LESS2 = "030028";
        let MID = "111100300000000";
        let MID_LESS2 = "1100300000000";
        let STAN = "000326";
        let TIMESTAMP = "66666666";
        let OP = "0";
        let CHANNEL = "1";
        let AMT = "00001234";
        let AMT_LESS2 = "001234";
        let PAYMENTACCEPT = "0001";
        let CURRENCY = "0";
        let OTRS = "ZXCVZXCV";
        let MERCHANT_NAME = "TEST_MERCHANTNAME";
        let CRC = "3063";
        let CRC_FOR_LESS2 = "0DB7";
        
        let model2 = NetsDynQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION)
            .tid(input: TID_LESS2)
            .mid(input: MID_LESS2)
            .stan(input: STAN)
            .timeStamp(input: TIMESTAMP)
            .op(input: OP)
            .channel(input: CHANNEL)
            .amt(input: AMT_LESS2)
            .paymentAccept(input: PAYMENTACCEPT)
            .currency(input:CURRENCY)
            .otrs(input:OTRS)
            .merchantName(input:MERCHANT_NAME)
            .checksum(input:CRC_FOR_LESS2)
            .build();
        
        let model1 = NetsDynQrModel.Builder()
            .header(input: HEADER)
            .version(input: VERSION)
            .tid(input: TID)
            .mid(input: MID)
            .stan(input: STAN)
            .timeStamp(input: TIMESTAMP)
            .op(input: OP)
            .channel(input: CHANNEL)
            .amt(input: AMT)
            .paymentAccept(input: PAYMENTACCEPT)
            .currency(input:CURRENCY)
            .otrs(input:OTRS)
            .merchantName(input:MERCHANT_NAME)
            .checksum(input:CRC)
            .build();
        
        let dynQr = HEADER + VERSION + TID + MID + STAN + TIMESTAMP + OP + CHANNEL + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC;
        let dynQr2 = HEADER + VERSION + "##" + TID_LESS2 + "##" + MID_LESS2 + STAN + TIMESTAMP + OP + CHANNEL + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC_FOR_LESS2;
        
        let dynQrErrNoTid = HEADER + VERSION + MID + STAN + TIMESTAMP + OP + CHANNEL + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC;
        let dynQrErrNoChannel = HEADER + VERSION + TID + MID + STAN + TIMESTAMP + OP + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC;
        print(dynQr);
        print(model2.toString())
        do {
            let model = try QrUtils.parseNetsDynQrString(qrData: dynQr);
            let model_parsed2 = try QrUtils.parseNetsDynQrString(qrData: dynQr2);
            
            XCTAssertEqual(HEADER, model.header);
            XCTAssertEqual(VERSION, model.version);
            XCTAssertEqual(TID, model.tid);
            XCTAssertEqual(MID, model.mid);
            XCTAssertEqual(STAN, model.stan);
            XCTAssertEqual(TIMESTAMP, model.timeStamp);
            XCTAssertEqual(OP, model.op);
            XCTAssertEqual(CHANNEL, model.channel);
            XCTAssertEqual(AMT, model.amt);
            XCTAssertEqual(PAYMENTACCEPT, model.paymentAccept);
            XCTAssertEqual(CURRENCY, model.currency);
            XCTAssertEqual(OTRS, model.otrs);
            XCTAssertEqual(MERCHANT_NAME, model.merchantName);
            XCTAssertEqual(CRC, model.checksum);
            
            XCTAssertEqual(dynQr, model1.toString());
            XCTAssertEqual(model.toString(), model1.toString());
            XCTAssertEqual(dynQr2, model2.toString());
            XCTAssertEqual(model_parsed2.toString(), model2.toString());
            
            XCTAssertEqual(HEADER, model_parsed2.header);
            XCTAssertEqual(VERSION, model_parsed2.version);
            XCTAssertEqual(TID_LESS2, model_parsed2.tid);
            XCTAssertEqual(MID_LESS2, model_parsed2.mid);
            XCTAssertEqual(STAN, model_parsed2.stan);
            XCTAssertEqual(TIMESTAMP, model_parsed2.timeStamp);
            XCTAssertEqual(OP, model_parsed2.op);
            XCTAssertEqual(CHANNEL, model_parsed2.channel);
            XCTAssertEqual(AMT, model_parsed2.amt);
            XCTAssertEqual(PAYMENTACCEPT, model_parsed2.paymentAccept);
            XCTAssertEqual(CURRENCY, model_parsed2.currency);
            XCTAssertEqual(OTRS, model_parsed2.otrs);
            XCTAssertEqual(MERCHANT_NAME, model_parsed2.merchantName);
            XCTAssertEqual(CRC_FOR_LESS2, model_parsed2.checksum);
            
            XCTAssertThrowsError(try QrUtils.parseNetsDynQrString(qrData: dynQrErrNoTid), "Error!");
            XCTAssertThrowsError(try QrUtils.parseNetsDynQrString(qrData: dynQrErrNoChannel), "Error!");
            
        } catch let err as QrConversionError {
            print(err.errorCode + err.errorMessage);
            XCTFail()
        } catch {
            XCTFail()
        }
    }
    
    func testEditableFromSgQr() throws {
        let editableData = "00020101021133860011SG.COM.NETS01231012345678918123123595902111113503610003083503610309011990810E602FC51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE6304eebb"
        let nonEditableData = "00020101021133860011SG.COM.NETS01231012345678918123123595902111113503610003083503610309010990810E602FC51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE6304eebb"
        let absentData = "00020101021133860011SG.COM.NETS012310123456789181231235959021111135036100030835036103990810E602FC51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE6304eebb"
        
        let sgModel = QrUtils.parseSgQrString(qrData: editableData)
        let netsModel1 = try QrUtils.convertSgQrModelToNetsQrModel(sgQrModel: sgModel)
        let netsModel2 = try QrUtils.convertSgQrStringToNetsQrModel(qrData: editableData)
        
        XCTAssertTrue(netsModel1.getTxnAmtQualifier() == "1" )
        XCTAssertTrue(netsModel2.getTxnAmtQualifier() == "1" )
        
        let sgModelNon = QrUtils.parseSgQrString(qrData: nonEditableData)
        let netsModel1Non = try QrUtils.convertSgQrModelToNetsQrModel(sgQrModel: sgModelNon)
        let netsModel2Non = try QrUtils.convertSgQrStringToNetsQrModel(qrData: nonEditableData)
        
        XCTAssertTrue(netsModel1Non.getTxnAmtQualifier() == "0" )
        XCTAssertTrue(netsModel2Non.getTxnAmtQualifier() == "0" )
        
        let sgModelAbsent = QrUtils.parseSgQrString(qrData: absentData)
        let netsModelAbsent1 = try QrUtils.convertSgQrModelToNetsQrModel(sgQrModel: sgModelAbsent)
        let netsModelAbsent2 = try QrUtils.convertSgQrStringToNetsQrModel(qrData: absentData)
        
        XCTAssertTrue(netsModelAbsent1.getTxnAmtQualifier() == nil )
        XCTAssertTrue(netsModelAbsent2.getTxnAmtQualifier() == nil )
    }
    
//    func testConvertSgQrAmtToNetsQrAmt() throws {
//                let noDecimal                   = "1234";
//                let withDecimal1dp              = "12.3";
//                let withDecimal2dp              = "12.34";
//                let withDecimal0dp              = "12.";
//                let notTooLargeNoDecimal        = "123456";
//                let notTooLargeWithDecimal      = "123456.78";
//                let manyLeadingZerosNoDecimal   = "000000001234";
//                let manyLeadingZerosWithDecimal = "0000001234.56";
//
//                let tooLargeNoDecimal   = "1234567";
//                let tooLargeWithDecimal = "1234567.89";
//                let tooLongNoDecimal    = "12345678901234";
//                let tooLongWithDecimal  = "1234567890.123";
//
//                do {
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: noDecimal));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: withDecimal1dp));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: withDecimal2dp));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: withDecimal0dp));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: notTooLargeNoDecimal));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: notTooLargeWithDecimal));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: manyLeadingZerosNoDecimal));
//                    print(try QrUtils.convertSgQrAmtToNetsQrAmt(amount: manyLeadingZerosWithDecimal));
//                } catch (let qce as QrConversionError) {
//                    print(qce.errorMessage)
//                    XCTFail();
//                }
//                do {
//                    try QrUtils.convertSgQrAmtToNetsQrAmt(amount: tooLargeNoDecimal);
//                    XCTFail();
//                    } catch (let ignored as QrConversionError) {
//                    }
//        do {
//            try QrUtils.convertSgQrAmtToNetsQrAmt(amount: tooLargeWithDecimal);
//            XCTFail();
//        } catch (let ignored as QrConversionError) {
//        }
//        do {
//            try QrUtils.convertSgQrAmtToNetsQrAmt(amount: tooLongNoDecimal);
//            XCTFail();
//        } catch (let ignored as QrConversionError) {
//        }
//        do {
//            try QrUtils.convertSgQrAmtToNetsQrAmt(amount: tooLongWithDecimal);
//            XCTFail();
//        } catch (let ignored as QrConversionError) {
//        }
//
//
//    }

    
    
}
