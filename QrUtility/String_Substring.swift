//
//  String_Substring.swift
//  QrUtility
//
//  Created by Jason Loh on 1/12/17.
//  Copyright © 2017 BixelTest. All rights reserved.
//

import Foundation

extension String {
    func getSubstring(from: Int, toBefore: Int) -> String {
        if(self.isEmpty ||
            from >= toBefore ||
            from < 0 ||
            toBefore > self.count
            ){
            return "";
        }
        let startIndex   = self.index(self.startIndex, offsetBy: from);
        let endIndex     = self.index(self.startIndex, offsetBy: toBefore);
        let subString    = self[startIndex ..< endIndex];
        
        return String(subString);
    }
}
